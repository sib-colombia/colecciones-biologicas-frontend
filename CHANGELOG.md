### Changelog 
# CHANGELOG Colecciones en línea

## [**1.1.0**] - (proximamente)

### Agregado
- Descargas de listas de especies.

#### Actualizado
- Actualizaciones de la interfaz visual.
- Mejoras de usabilidad y experiencia de usuario.
#### Solucionado
- Conteo del *Home*.

#### Eliminado
- Pestaña de mapa.
- Pestaña de especies.
- Columna de recursos en la pestaña de recursos
- Filtro de elevación.
- Filtro de fecha del evento.
- Filtro de profundidad.
- Barra de búsqueda superior.

## [**1.0.0**] – (2018-06)
### Agregado
- sección de búsqueda por registros biológicos:
    - Filtros:
        - Taxonomía
        - Categoría taxonómica
        - Ubicación
        - Hábitat
        - Base del registro
        - Registrado por
        - ID del registro biológico
        - Publicador
        - Redes y proyectos
        - Nombre del recurso
        - Multimedia
    - Pestañas:
        - Registros
        - Galería
        - Recursos
        - Publicadores
- Sección de búsqueda por colección:
    - Filtros:
        - Nombre del recurso
        - Publicador
        - Licencia
        - Fecha del evento
        - Doi
- Sección de búsqueda por publicadores:
    - Filtros:
        - Publicador
        - Redes y proyectos
- Vista detalle por colección.
- Vista detalle de publicador.
- Conexión con nuevo API de consultas.
- Conexión con nuevo API de búsquedas.
