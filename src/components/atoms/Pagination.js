import React, { Component } from 'react';
import _ from 'lodash';

class Pagination extends Component {
  static defaultProps = {
    initialPage: 1
  }

  constructor(props) {
    super(props);
    this.state = {
      pager: {}
    }
  }

  componentWillMount() {

    const { items } = this.props;
    if (items && items > 0) {
      this.setPage(this.props.initialPage);
    }
  }

  componentDidMount() {
    const { items } = this.props;
    if (items) {
      this.setPage(this.props.initialPage);
    }
  }

  setPage(page, setProp = false) {
    const { items, pageSize } = this.props;
    let { pager } = this.state;

    if ((page < 1 || page > pager.totalPages) && page > pager.currentPage && !this.props.next) {
      return;
    }
    pager = this.getPager(items, page, pageSize);

    this.setState({ pager: pager });

    setProp && this.props.onChangePage(pager.currentPage);
  }

  getPager(totalItems, currentPage, pageSize) {
    currentPage = currentPage || 1;

    pageSize = pageSize || 20;

    const totalPages = Math.ceil(totalItems / pageSize);

    let startPage,
      endPage;
    if (totalPages <= 10) {
      startPage = 1;
      endPage = totalPages;
    } else {
      if (currentPage <= 6) {
        startPage = 1;
        endPage = 10;
      } else if (currentPage + 4 >= totalPages) {
        startPage = totalPages - 9;
        endPage = totalPages;
      } else {
        startPage = currentPage - 5;
        endPage = currentPage + 4;
      }
    }

    const startIndex = (currentPage - 1) * pageSize;
    const endIndex = Math.min(startIndex + pageSize - 1, totalItems - 1);

    const pages = _.range(startPage, endPage + 1);

    return {
      totalItems: totalItems,
      currentPage: currentPage,
      pageSize: pageSize,
      totalPages: totalPages,
      startPage: startPage,
      endPage: endPage,
      startIndex: startIndex,
      endIndex: endIndex,
      pages: pages
    };
  }

  render() {
    let { pager } = this.state;

    if (this.props.next !== undefined) {
      if (!pager.pages && this.props.next) {
        pager.pages = [1, 2]
      } else {
        pager.pages = []
        let limite = 1
        if (this.state.pager.currentPage !== undefined) {
          limite = this.state.pager.currentPage;
        }
        let m = 0;
        if (this.props.next) {
          m = 1
        }
        for (let i = 0; i < limite + m; i++) {
          pager.pages.push(i + 1);
        }
      }
    }

    if ((!pager.pages || pager.pages.length <= 1) && !this.props.next) {
      return null;
    }

    return (<ul className="uk-pagination uk-flex-center" data-uk-margin="data-uk-margin">
      <li className={pager.currentPage === 1
        ? 'uk-disabled'
        : ''}>
        <a title="Anterior" data-uk-tooltip="" onClick={() => this.setPage(pager.currentPage - 1, true)} data-uk-scroll="">
          <span uk-pagination-previous=""></span>
        </a>
      </li>
      {
        pager.pages.map((page, index) => <li key={index} className={pager.currentPage === page
          ? 'uk-active'
          : ''}>
          <a onClick={() => this.setPage(page, true)} data-uk-scroll="">{page}</a>
        </li>)
      }
      {
        this.props.next &&
        <span>...</span>
      }
      <li className={pager.currentPage === pager.totalPages
        ? 'uk-disabled'
        : ''}>
        <a title="Siguiente" data-uk-tooltip="" onClick={() => this.setPage(pager.currentPage + 1, true)} data-uk-scroll="">
          <span uk-pagination-next=""></span>
        </a>
      </li>
    </ul>);
  }
}

export default Pagination;
