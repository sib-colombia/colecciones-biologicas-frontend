import React, { Component } from 'react';

class Title extends Component {
  render() {
    return (
      <div className="uk-flex uk-flex-column">
        <h4 className="uk-margin-left">
          <span className="uk-text-bold">{this.props.label}</span>
          {this.props.icon}
          <span className="uk-text-bold">{this.props.number}</span> <span className="uk-text-small">{this.props.labelNumber}</span>
        </h4>
        <div className="uk-width-1-4 uk-width-small@s uk-background-primary" style={{ height: 2 }}></div>
      </div>
    );
  }
};

export default Title;
