import React, { Component } from 'react';
import ReactAutocomplete from 'react-autocomplete';

class Autocomplete extends Component {

  constructor() {
    super();
    this.state = {
      value: ''
    }
  }

  selectItem(value) {
    this.props.selectValue(value);
    this.setState({ value: '' });
  }

  handleChange(e) {
    e = e.target.value
    this.setState({ value: e}, () => this.props.onChange(e))
  }

  render() {
    return (
      <ReactAutocomplete
        items={this.props.values}
        shouldItemRender={(item, value) => item.label.toLowerCase().indexOf(value.toLowerCase()) > -1}
        getItemValue={item => item.label}
        renderItem={(item, highlighted) =>
          <div
            key={item.id}
            style={{ backgroundColor: highlighted ? '#297bce' : '#fff', color: highlighted ? '#fff' : '#666', paddingLeft: 10 }}
          >
            {item.label}
          </div>
        }
        value={this.state.value}
        onChange={(e) => this.handleChange(e)}
        onSelect={value => this.selectItem(value)}
        wrapperStyle={{ width: '100%' }}
        renderInput={(props) => (
          <input className="uk-input uk-form-small" type="text" placeholder={this.props.placeholder} {...props} />
        )}
        menuStyle={{
          boxShadow: '1px 1px 7px rgba(0, 0, 0, 0.7)',
          background: 'rgb(255, 255, 255)',
          fontSize: '90%',
          overflow: 'auto',
        }}
      />
    );
  }
}

export default Autocomplete;
