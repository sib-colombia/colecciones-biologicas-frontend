import React, { Component } from 'react';
import { Switch, Route } from 'react-router-dom';
import UIkit from 'uikit';
import Icons from 'uikit/dist/js/uikit-icons';

import Home from './pages/Home';
import Search from './pages/Search';
import Occurrence from './pages/details/Occurrence';
import Providers from './pages/Providers';
import Provider from './pages/details/Provider';
import Collections from './pages/Collections';
import Collection from './pages/details/Collection';
import NotFound from './pages/NotFound';
import Static from './pages/Static';

UIkit.use(Icons);

class App extends Component {

  render() {
    return (
      <Switch>
        <Route exact path="/" component={Home} />
        <Route path="/search/:tab?" component={Search} />

        <Route path="/occurrence/:id" component={Occurrence} />

        <Route path="/providers" component={Providers} />
        <Route path="/provider/:id" component={Provider} />

        <Route path="/collections" component={Collections} />
        <Route path="/collection/:id" component={Collection} />
        
        <Route path="/static/:id" component={Static} />

        <Route component={NotFound} />
      </Switch>
    );
  }
}

export default App;
