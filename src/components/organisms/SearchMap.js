import React, { Component } from 'react';
import { map } from 'lodash';
import MarkerClusterGroup from 'react-leaflet-markercluster';

import HumboldtMap from '../molecules/HumboldtMap';
import * as MapService from '../../services/MapService';


class SearchMap extends Component {

  constructor(props) {
    super(props);
    this.state = {
      data: null
    }
  }

  componentWillMount() {
    this.props.onRef(this)
  }

  componentDidMount() {
    this.offsetResults(0);
  }

  offsetResults(offset, pageOfItems, search) {
    if (search === undefined)
      search = this.props.search
    if(search.indexOf('?') < 0)
      search = '?' + search

    MapService.getOccurrenceList('', search)
      .then(data => {
        if (data.values !== undefined && data.values.length > 0) {
          data = data.values
          let markers = []
          map(data, (v, k) => {
            let lat = parseFloat(v.decimalLatitude)
            let lon = parseFloat(v.decimalLongitude)

            if (isNaN(lat) || isNaN(lon)) {
              console.log("El registro no tiene bien los puntos:", k)
              console.log(v)
            } else {
              markers[k] = {
                position: [
                  parseFloat(lat), parseFloat(lon)
                ],
                popup: (`
                    <div>
                      <h4 class="uk-margin-remove">Registro</h4>
                      <p class="uk-heading-divider uk-margin-small"><a href="/occurrence/${v.id}" target="_blank" class="uk-link-reset" >${v.scientificName}</a></p>
                      <h4 class="uk-margin-remove">Recurso</h4>
                      <p class="uk-heading-divider uk-margin-small"><a href="/dataset/${v.GBIFID}" target="_blank" class="uk-link-reset" >${v.resource_name}</a></p>
                      <h4 class="uk-margin-remove">Publicador</h4>
                      <p class="uk-margin-small"><a href="/provider/${v.provider_id}" target="_blank" class="uk-link-reset" >${v.provider_name}</a></p>
                    </div>
                  `)
              }
            }
          })
          this.setState({ data: markers })
        } else {
          //alert("No se encontraron puntos")
        }
      })
  }

  render() {
    return (
      <HumboldtMap tipo="occurrences" center={[4.36, -74.04]} zoom={6} minHeigth={800} >
        {this.state.data && <MarkerClusterGroup markers={this.state.data} />}
      </HumboldtMap>
    )
  }
}

export default SearchMap;
