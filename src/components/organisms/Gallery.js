import React, { Component } from 'react';
import StackGrid from "react-stack-grid";
import map from 'lodash/map';
import Lightbox from 'react-image-lightbox';
import ReactAudioPlayer from 'react-audio-player';

import GalleryItem from '../molecules/GalleryItem';
import Pagination from '../atoms/Pagination';
import License from '../atoms/License';

import * as OccurrenceService from '../../services/OccurrenceService';



export class Gallery extends Component {
  render() {
    return (
      <div className="uk-section uk-section-small">
        <div className="uk-container uk-width-5-6@m uk-width-2-3@l">
          <div className="uk-flex uk-flex-column">
            <h3 className="uk-margin-left uk-text-bold">{this.props.title}</h3>
            <div className="uk-width-1-4 uk-width-small@s uk-background-primary" style={{ height: 2 }} />
          </div>
          {this.props.children}
        </div>
      </div>
    )
  }
}

export class Tab extends Component {

  constructor() {
    super();
    this.state = {
      data: null,
    }
  }

  componentDidMount() {
    this.offsetResults(0);
  }

  onChangePage(pageOfItems) {
    const page = pageOfItems * 20;
    this.offsetResults(page, pageOfItems);
  }

  render() {
    // const { data } = this.props;

    return (
      <div>
        <div className="uk-child-width-1-2 uk-child-width-1-3@s uk-child-width-1-4@m uk-child-width-1-5@l uk-grid-small uk-grid-match" data-uk-grid>
          {/* {data && map(data, (dataset, key) => (<GalleryItem key={key} data={dataset} />))} */}
          <GalleryItem />
          <GalleryItem />
          <GalleryItem />
          <GalleryItem />
          <GalleryItem />
        </div>
        {this.state.data && <Pagination initialPage={this.state.currentPage} items={this.state.count} onChangePage={(number) => {this.onChangePage(number - 1);}} />
        }
      </div>
    )
  }
}

export class Box extends Component {
  render() {
    const { imgf, photoIndex } = this.props;
    return (
      <Lightbox
        mainSrc={imgf[photoIndex].identifier}
        nextSrc={imgf[(photoIndex + 1) % imgf.length].identifier}
        prevSrc={imgf[(photoIndex + imgf.length - 1) % imgf.length].identifier}

        onCloseRequest={() => this.props.close({ isOpen: false })}
        onMovePrevRequest={() => this.props.index({
          photoIndex: (photoIndex + imgf.length - 1) % imgf.length,
        })}
        onMoveNextRequest={() => this.props.index({
          photoIndex: (photoIndex + 1) % imgf.length,
        })}
      />
    )
  }
}

export class Section extends Component {
  render() {
    const { snapshots, columnWidth } = this.props;
    return (
      <StackGrid
        columnWidth={columnWidth}
        monitorImagesLoaded
        className="uk-margin-top"
        gutterWidth={15}
        gutterHeight={15}
      >
        {
          map(snapshots, (img, key) => (
            <div key={key} className="uk-card uk-card-default uk-box-shadow-small uk-box-shadow-hover-medium">
              <img className="" key={key} src={OccurrenceService.getThumb(img.identifier)} alt="" onClick={() => this.props.photoIndex(key)} style={{ cursor: 'pointer' }} />
              <div className="uk-padding-small uk-grid-collapse uk-child-width-1-1" data-uk-grid="" data-uk-margin="small">
                {img.creator && <div><span className="uk-text-bold uk-margin-small-right">Creador:</span>{img.creator}</div>}
                {img.publisher && <div><span className="uk-text-bold uk-margin-small-right">Publicador:</span>{img.publisher}</div>}
                {img.license && <div><License id={img.license} /></div>}
                {img.references && <div className="uk-text-truncate"><span className="uk-text-bold uk-margin-small-right">Referencia:</span><a href={img.references} target="_blank" >{img.references}</a></div>}
              </div>
            </div>
          ))
        }
      </StackGrid>
    )
  }
}

export class Sound extends Component {
  render() {
    const { sounds, columnWidth } = this.props;
    return (
      <StackGrid
        columnWidth={columnWidth}
        monitorImagesLoaded
        className="uk-margin-top"
        gutterWidth={15}
        gutterHeight={15}
      >
        {
          map(sounds, (sound, key) => (

            <div key={key} className="uk-card uk-card-default uk-box-shadow-small uk-box-shadow-hover-medium">
              <div className="uk-padding-small">
                {sound.title && <h5 className="uk-text-bold">{sound.title}</h5>}
              </div>
              <div className="uk-padding-small uk-padding-remove-top uk-padding-remove-bottom">
                <ReactAudioPlayer
                  src={sound.identifier}
                  controls
                />
              </div>
              <div className="uk-padding-small uk-padding-remove-top uk-grid-collapse uk-child-width-1-1" data-uk-grid="" data-uk-margin="small">
                {sound.rightsHolder && <div>{sound.rightsHolder}</div>}
                {sound.creator && <div><span className="uk-text-bold uk-margin-small-right">Creador:</span>{sound.creator}</div>}
                {sound.publisher && <div><span className="uk-text-bold uk-margin-small-right">Publicador:</span>{sound.publisher}</div>}
                {sound.license && <div><License id={sound.license} /></div>}
                {sound.references && <div className="uk-text-truncate"><span className="uk-text-bold uk-margin-small-right">Referencia:</span><a href={sound.references} target="_blank" >{sound.references}</a></div>}
              </div>
            </div>
          ))
        }
      </StackGrid>
    )
  }
}

export class Video extends Component {
  render() {
    const { videos, columnWidth } = this.props;
    console.log(videos)
    return (
      <StackGrid
        columnWidth={columnWidth}
        monitorImagesLoaded
        className="uk-margin-top"
        gutterWidth={15}
        gutterHeight={15}
      >
        {
          map(videos, (video, key) => (

            <div key={key} className="uk-card uk-card-default uk-box-shadow-small uk-box-shadow-hover-medium">
              <div className="uk-padding-small">
                {video.title && <h5 className="uk-text-bold">{video.title}</h5>}
              </div>
              <div className="uk-padding-small uk-padding-remove-top uk-padding-remove-bottom">
                <video controls>
                  <source src={video.identifier} type={video.format} />
                </video>
              </div>
              <div className="uk-padding-small uk-padding-remove-top uk-grid-collapse uk-child-width-1-1" data-uk-grid="" data-uk-margin="small">
                {video.rightsHolder && <div>{video.rightsHolder}</div>}
                {video.creator && <div><span className="uk-text-bold uk-margin-small-right">Creador:</span>{video.creator}</div>}
                {video.publisher && <div><span className="uk-text-bold uk-margin-small-right">Publicador:</span>{video.publisher}</div>}
                {video.license && <div><License id={video.license} /></div>}
                {video.references && <div className="uk-text-truncate"><span className="uk-text-bold uk-margin-small-right">Referencia:</span><a href={video.references} target="_blank" >{video.references}</a></div>}
              </div>
            </div>
          ))
        }
      </StackGrid>
    )
  }
}

Gallery.Tab = Tab;
Gallery.Box = Box;
Gallery.Section = Section;
Gallery.Sound = Sound;
Gallery.Video = Video;

export default Gallery;
