import React, { Component } from 'react';
import _ from 'lodash';

import OccurrenceRow from '../molecules/OccurrenceRow';
import Pagination from '../atoms/Pagination';
import * as OccurrenceService from '../../services/OccurrenceService';
import Loading from '../atoms/Loading';

class OccurrenceTable extends Component {

  constructor(props) {
    super(props);
    this.state = {
      data: null,
      count: NaN,
      show: false,
      search: ''
    }
  }

  componentWillMount() {
    if (this.props.onRef !== undefined) {
      this.props.onRef(this)
    }
  }

  componentDidMount() {
    this.offsetResults(0);
  }

  onChangePage(pageOfItems) {
    const page = pageOfItems * 20;
    this.offsetResults(page, pageOfItems);
  }

  offsetResults(offset, pageOfItems, search) {
  
    if (search === undefined)
      search = this.props.search
      
    this.setState({ data: null, search }, () => {
      OccurrenceService.ESgetOccurrenceList(offset, search).then(data => {
        this.setState({
          data: data.hits.hits,
          offset: offset,
          count: data.hits.total,
          currentPage: pageOfItems + 1
        })

        this.props.occurrences(data.hits.total);
      });
    })
  }

  render() {
    return (
      <li>
        {(this.state.data && <div className="uk-card uk-card-default uk-overflow-auto uk-margin">
          <table className="uk-table uk-table-divider uk-table-small uk-table-hover">
            <thead>
              <tr>
                <th>Imágen</th>
                <th className="uk-text-nowrap">Nombre Científico</th>
                <th>Número del Catálogo</th>
                <th>Colección</th>
                <th>Publicador</th>
              </tr>
            </thead>
            <tbody>
              {_.map(this.state.data, (occurrence, key) => (<OccurrenceRow key={key} data={occurrence} />))}
            </tbody>
          </table>
        </div>) || <Loading />}
        {this.state.data && <Pagination initialPage={this.state.currentPage} items={this.state.count} onChangePage={(number) => { this.onChangePage(number - 1); }} />}
      </li>
    )
  }
}

export default OccurrenceTable;

