import React, {Component} from 'react';
import map from 'lodash/map';

import CollectionListItem from '../molecules/CollectionListItem';
import Loading from '../atoms/Loading';

class CollectionsList extends Component {
  render() {
    const {data} = this.props;
    return (<div className="uk-container uk-container-small">
      {(data && <div className="uk-flex uk-flex-column">
        {map(data, (dataset, key) => (<CollectionListItem key={key} data={dataset}/>))}
      </div>) || <Loading />}
    </div>)
  }
}

export default CollectionsList;
