import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { URL_LISTAS, URL_PORTAL, URL_CATALOGO } from '../../config/const';

class Footer extends Component {
  render() {
    return (<div>
      <div className="uk-section uk-section-small" style={{
        backgroundColor: '#00484d'
      }}>
        <div className="uk-container uk-width-5-6@m uk-width-2-3@l">
          <div className="uk-child-width-1-1 uk-child-width-1-2@s uk-child-width-1-4@m" data-uk-grid="">
            <div className="uk-text-center uk-text-left@m">
              <a href={URL_CATALOGO} target="_blank" rel="noopener noreferrer">
                <img src="https://statics.sibcolombia.net/sib-resources/images/logos-canales/png/logo-catalogo-b.png" width="200" alt="" />
              </a>
            </div>
            <div className="uk-text-center">
              <a href={URL_PORTAL} target="_blank" rel="noopener noreferrer">
                <img src="https://statics.sibcolombia.net/sib-resources/images/logos-canales/png/logo-datos-b.png" width="200" alt="" />
              </a>
            </div>
            <div className="uk-text-center">
              <Link to="">
                <img src="https://statics.sibcolombia.net/sib-resources/images/logos-canales/png/logo-colecciones-b.png" width="200" alt="" />
              </Link>
            </div>
            <div className="uk-text-center uk-text-right@m">
              <a href={URL_LISTAS} target="_blank" rel="noopener noreferrer">
                <img src="https://statics.sibcolombia.net/sib-resources/images/logos-canales/png/logo-listas-b.png" width="200" alt="" />
              </a>
            </div>
          </div>
        </div>

      </div>

      <div className="uk-section uk-section-xsmall" style={{
        backgroundColor: '#00292b'
      }}>
        <div className="uk-container uk-width-5-6@m uk-width-2-3@l">
          <div className="uk-child-width-expand@s uk-flex uk-flex-middle" data-uk-grid="data-uk-grid">
            <div className="uk-text-center uk-text-left@s">
              <a href="">
                <img src="/static/css/images/sib-logo.svg" alt="" width="300" />
              </a>
            </div>
            <div >
              <div className="uk-flex uk-flex-center uk-flex-right@s uk-flex-middle uk-light uk-grid-small">
                <div>
                  <Link to="/static/sobre_el_portal">Sobre el portal</Link>
                </div>
                <div className="uk-text-lead">|</div>
                <div>
                  <Link to="/static/condiciones_de_uso">Condiciones de uso</Link>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      {/*
      <div className="uk-section uk-section-xsmall uk-section-secondary uk-padding-top-remove uk-text-center" style={{
        backgroundColor: '#000000',
        paddingTop: 5,
        paddingBottom: 5
      }}>
        Creative Commons 4.0 · 2015
      </div>*/}
    </div>);
  }
}

export default Footer;
