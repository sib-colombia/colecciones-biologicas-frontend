import React, { Component } from 'react';
import _ from 'lodash';

import GalleryCard from '../molecules/GalleryCard';
import Pagination from '../atoms/Pagination';
import * as OccurrenceService from '../../services/OccurrenceService';
import Loading from '../atoms/Loading';

import StackGrid from "react-stack-grid";

class CollectionsGallery extends Component {

  constructor(props) {
    super(props);
    this.state = {
      data: null,
      count: NaN,
      show: false,
      search: ''
    }
  }

  componentWillMount() {
    if (this.props.onRef !== undefined) {
      this.props.onRef(this)
    }
  }

  componentDidMount() {
    this.offsetResults(0);
  }

  onChangePage(pageOfItems) {
    const page = pageOfItems * 20;
    this.offsetResults(page, pageOfItems);
  }

  offsetResults(offset, pageOfItems, search) {
  
    if (search === undefined)
      search = this.props.busqueda
    search = "mediaType=StillImage"+(search!==undefined?"&"+search:"")
    this.setState({ data: null, search }, () => {
      OccurrenceService.ESgetOccurrenceList(offset, search).then(data => {
        this.setState({
          data: data.hits.hits,
          offset: offset,
          count: data.hits.total,
          currentPage: pageOfItems
        })

        this.props.gallerys(data.count);
      });
    })
  }
  

  render() {
    console.log(this.state.data)
    return (
      <li>
        {(this.state.data && 
          <StackGrid columnWidth={"25%"} monitorImagesLoaded gutterWidth={20} gutterHeight={20}>
            {_.map(this.state.data, (occurrence, key) => (<GalleryCard  key={key} occurrence={occurrence} />))}
          </StackGrid>) || <Loading />}
        {this.state.data && <Pagination items={this.state.count} onChangePage={(number) => { this.onChangePage(number - 1); }} />}
      </li>
    )
  }
}

export default CollectionsGallery;
