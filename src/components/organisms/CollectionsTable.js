import React, { Component } from 'react';
import map from 'lodash/map';

import DatasetsRow from '../molecules/CollectionsRow';
import Pagination from '../atoms/Pagination';
import Loading from '../atoms/Loading';
import * as CollectionsService from '../../services/CollectionsService';

class DatasetsTable extends Component {

  constructor(props) {
    super(props);
    this.state = {
      data: null,
      count: NaN,
      show: false,
      buffer: null,
      limite: 20,
      search: ""
    }
  }

  componentWillMount() {
    this.props.onRef(this)
  }

  componentDidMount() {
    this.offsetResults();
  }

  onChangePage(limite) {
    this.offsetResults(limite);
  }

    cargarMas(){
    this.setState({limite: this.state.limite+20}, ()=>{
      this.offsetResults()
    })
  }

  offsetResults(search) {
    if (search === undefined)
      search = this.props.search

    CollectionsService.getDatasetList(this.state.limite, search).then(data => {
      this.setState({
        data: data.aggregations.gbifId.buckets,
        count: data.aggregations.gbifId.doc_count_error_upper_bound,
        actualPage: 0
      })
      this.props.collections(data.count);
    });
  }

  render() {

    let t
    if (this.state.data===null){
    t = 0
    }else{
      t = this.state.data.length
    }

    return (
    <li>
      {(this.state.data && <div className="uk-card uk-card-default uk-overflow-auto uk-margin">
        <table className="uk-table uk-table-divider uk-table-small uk-table-hover">
          <thead>
            <tr>
              <th className="uk-width-large">Nombre del recurso</th>
              <th className="uk-width-small">Registros</th>
              <th>Publicador</th>
            </tr>
          </thead>
          <tbody>
            {this.state.data && map(this.state.data, (dataset, key) => (<DatasetsRow key={key} data={dataset} />))}
          </tbody>
        </table>
      </div>) || <Loading />}
      {t===this.state.limite &&
        <div className="uk-align-center uk-button uk-button-primary" onClick={()=>{this.cargarMas()}}>Cargar más de {t} {this.state.cargando}</div>
      }
    </li>
    )
  }
}

export default DatasetsTable;
