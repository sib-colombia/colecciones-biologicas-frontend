import React, { Component } from 'react';
import map from 'lodash/map';

import ProvidersListItem from '../molecules/ProvidersListItem';
import Loading from '../atoms/Loading';

export class ProvidersList extends Component {
  render() {
    const { data } = this.props;
    return (
      <div className="uk-container uk-container-small">
        {(data && <div className="uk-flex uk-flex-column">
          {map(data, (dataset, key) => (<ProvidersListItem key={key} data={dataset}/>))}
        </div>) || <Loading />}
      </div>
    );
  }
}

export default ProvidersList;
