import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { indexOf } from 'lodash';

import Search from '../molecules/Search';
import { URL_LOGO } from '../../config/const';


class Header extends Component {

  constructor() {
    super();
    this.toggleMenu = this.toggleMenu.bind(this)
    this.openAdvanceSearch = this.openAdvanceSearch.bind(this)
  }

  toggleMenu() {
    this.props.menu()
  }

  openAdvanceSearch() {
    this.props.openAdvanceSearch()
  }

  preventHideMenu(e) {
    const ids = ['username', 'password', 'email', 'search'];
    const tags = ['INPUT', 'LABEL', 'A'];
    if (indexOf(tags, e.target.tagName) < 0 || indexOf(ids, e.target.id) >= 0) {
      e.preventDefault()
    }
  }

  render() {
    return (
      <div id="navbar" onClick={this.preventHideMenu} className="uk-box-shadow-small uk-background-default" uk-sticky="sel-target: !nav; cls-active: uk-navbar-sticky">
        <nav data-uk-navbar="mode: hover">
          {
            this.props.withSidebar && <div className="uk-animation-slide-left uk-background-primary uk-navbar-left">
              <button
                data-uk-toggle="target: #my-id"
                className="uk-margin-small-left uk-margin-small-right uk-navbar-toggle uk-text-default"
                uk-navbar-toggle-icon=""
                onClick={this.toggleMenu}
              />
            </div>
          }
          <div className="uk-margin-small-left uk-navbar-left uk-visible@s">
            <button className="uk-navbar-toggle uk-hidden@s" uk-navbar-toggle-icon="" />
            <Link className="uk-logo" to="/">
              <img src={URL_LOGO} alt="" style={{ maxWidth: 'none' }} />
            </Link>
          </div>
          <div className="uk-margin-small-left uk-navbar-center uk-hidden@s">
            <Link className="uk-logo" to="/">
              <img src={URL_LOGO} alt="" style={{ maxWidth: 'none' }} />
            </Link>
          </div>
          {/*<Search openAdvanceSearch={this.openAdvanceSearch} search={this.props.search} />*/}
          <div className="uk-navbar-right">
            <button className="uk-navbar-toggle uk-hidden@m" uk-search-icon="" />
            <Search.Responsive />
            <ul className="uk-navbar-nav uk-visible@s">
              <li>
                <a className="uk-text-bold">Explorar<span data-uk-icon="icon: triangle-down"></span></a>
                <div className="uk-navbar-dropdown uk-margin-remove-top uk-padding-small">
                  <ul className="uk-nav uk-navbar-dropdown-nav">
                    <li><Link to="/search">Especímenes</Link></li>
                    <li><Link to="/collections">Colecciones</Link></li>
                    <li><Link to="/providers">Publicadores</Link></li>
                  </ul>
                </div>
              </li>
              {/*
              <li className="uk-margin-small-right">
                <a className="uk-text-bold">Ingresar</a>
                <div data-uk-drop="mode: hover; offset: 10">
                  <div className="uk-card uk-card-default">
                    <div className="uk-padding-small">
                      <ul className="uk-child-width-expand" data-uk-tab="connect: .switcher-log">
                        <li><a>Iniciar sesión</a></li>
                        <li><a>Registro</a></li>
                      </ul>
                      <ul className="uk-switcher switcher-log">
                        <li>
                          <form className="uk-form-stacked">
                            <div className="uk-margin-small">
                              <label className="uk-form-label" htmlFor="username">Usuario o correo electrónico</label>
                              <div className="uk-form-controls">
                                <input className="uk-input" id="username" type="text" />
                              </div>
                            </div>
                            <div>
                              <label className="uk-form-label" htmlFor="password">Contraseña</label>
                              <div className="uk-form-controls">
                                <input className="uk-input" id="password" type="password" />
                              </div>
                              <a className="uk-text-tertiary uk-text-small uk-float-right" href="">¿Olvidaste tu contraseña?</a>
                            </div>
                            <button className="uk-margin-small-top uk-button uk-button-small uk-text-bold uk-button-primary uk-width-1">Ingresar</button>
                          </form>
                        </li>
                        <li>
                          <form className="uk-form-stacked">
                            <div className="uk-margin-small">
                              <label className="uk-form-label" htmlFor="email">Correo electrónico</label>
                              <div className="uk-form-controls">
                                <input className="uk-input" id="email" type="text" />
                              </div>
                            </div>
                            <div className="uk-margin-small">
                              <label className="uk-form-label" htmlFor="username">Usuario</label>
                              <div className="uk-form-controls">
                                <input className="uk-input" id="username" type="text" />
                              </div>
                            </div>
                            <div className="uk-margin-small">
                              <label className="uk-form-label" htmlFor="password">Contraseña</label>
                              <div className="uk-form-controls">
                                <input className="uk-input" id="password" type="password" />
                              </div>
                            </div>
                            <button className="uk-button uk-button-small uk-text-bold uk-button-primary uk-width-1">Siguiente</button>
                          </form>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </li>*/}
            </ul>
          </div>
        </nav>
        {/* <Subnav /> */}
      </div>
    );
  }
}

export default Header;
