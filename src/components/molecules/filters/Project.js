import React, { Component } from 'react';
import { map, differenceWith, isEqual, findKey } from 'lodash';

import Filters from '../Filters';

class Project extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: null
    };

    this.filters = [
    //keywork
      { id: 'projectSib=RNOA', label: 'Red Nacional Observadores de Aves'  , idQ: 'RNOA', labelQ: 'projectSib', value: '' },
      { id: 'projectSib=VALLE_DEL_CAUCA', label: 'Datos de Biodiversidad Valle del Cauca'  , idQ: 'VALLE DEL CAUCA', labelQ: 'projectSib', value: '' },
      { id: 'projectSib=SIB MARINO', label: 'Sistema de Información sobre Biodiversidad Marina'  , idQ: 'SIB MARINO', labelQ: 'projectSib', value: '' },
      { id: 'projectSib=RESNATUR', label: 'Asociación Red Colombiana de Reservas Naturales de la Sociedad Civil'  , idQ: 'RESNATUR', labelQ: 'projectSib', value: '' },
      { id: 'projectSib=RNJB', label: 'Red Nacional de Jardines Botánicos de Colombia'  , idQ: 'RNJB', labelQ: 'projectSib', value: '' },
      { id: 'projectSib=COLOMBIA BIO', label: 'Colombia BIO'  , idQ: 'COLOMBIA BIO', labelQ: 'projectSib', value: '' },
      { id: 'projectSib=BST', label: 'Bosque Seco Tropical'  , idQ: 'BST', labelQ: 'projectSib', value: '' },
      { id: 'projectSib=IFN', label: 'Inventario Forestal Nacional'  , idQ: 'IFN', labelQ: 'projectSib', value: '' },
      { id: 'projectSib=SIRAP-EC', label: 'Sistemas Regional de Áreas Protegidas del Eje Cafetero'  , idQ: 'SIRAP-EC', labelQ: 'projectSib', value: '' },
      /*
      { id: 'keywork=RNOA', label: 'Red Nacional Observadores de Aves'  , idQ: 'RNOA', labelQ: 'keywork', value: '' },
      { id: 'keywork=VALLE DEL CAUCA', label: 'Datos de Biodiversidad Valle del Cauca'  , idQ: 'VALLE DEL CAUCA', labelQ: 'keywork', value: '' },
      { id: 'keywork=SIB MARINO', label: 'Sistema de Información sobre Biodiversidad Marina'  , idQ: 'SIB MARINO', labelQ: 'keywork', value: '' },
      { id: 'keywork=RESNATUR', label: 'Asociación Red Colombiana de Reservas Naturales de la Sociedad Civil'  , idQ: 'RESNATUR', labelQ: 'keywork', value: '' },
      { id: 'keywork=RNJB', label: 'Red Nacional de Jardines Botánicos de Colombia'  , idQ: 'RNJB', labelQ: 'keywork', value: '' },
      { id: 'keywork=COLOMBIA BIO', label: 'Colombia BIO'  , idQ: 'COLOMBIA BIO', labelQ: 'keywork', value: '' },
      { id: 'keywork=BST', label: 'Bosque Seco Tropical'  , idQ: 'BST', labelQ: 'keywork', value: '' },
      { id: 'keywork=IFN', label: 'Inventario Forestal Nacional'  , idQ: 'IFN', labelQ: 'keywork', value: '' },
      { id: 'keywork=SIRAP-EC', label: 'Sistemas Regional de Áreas Protegidas del Eje Cafetero'  , idQ: 'SIRAP-EC', labelQ: 'keywork', value: '' },
      */
    ];

    this.query = [];
  }

  componentWillMount() {
    this.activeFilters(this.props.activeFilters);
    this.props.onRef(this)
  }

  handleFilter(value) {
    this.query.push(value)
    this.props.count(this.query);
    this.setState({ data: this.query })
  }

  deleteFilter(value) {
    const position = findKey(this.query, value);
    this.query.splice(position, 1)
    this.props.count(this.query);
    this.setState({ data: this.query })
  }

  cleanFilters() {
    this.query = []
    this.child.handleDrop(false);
    this.props.count(this.query);
    this.setState({ data: this.query })
  }

  activeFilters(data) {
    map(data, (item) => {
      const i = findKey(this.filters, (o) => { return o.id === item })
      if (i >= 0) {
        this.handleFilter(this.filters[i])
      }
    })
  }

  render() {
    return (
      <Filters.Base onRef={ref => { this.child = ref }} title="Redes y proyectos" handlerFilter={this.state.data} func={(value) => this.deleteFilter(value)}>
        <div className="uk-child-width-1-1 uk-grid-collapse" data-uk-grid="">
        {
          map((differenceWith(this.filters, this.query, isEqual)), (value, key) =>
            <label
              key={key}
              onClick={(e) => { e.preventDefault(); this.handleFilter(value) }}
              style={{ cursor: 'pointer', marginBottom: 5 }}
              className="uk-grid-collapse uk-flex-between uk-flex-middle uk-text-small"
              data-uk-grid
              title={value.label}
            >
              <input
                className="uk-checkbox"
                type="checkbox"
              />
              <span className="uk-width-3-4 uk-text-truncate uk-margin-small-left">{value.label}</span>
              <span className="uk-width-expand uk-text-right">{value.value}</span>
            </label>
          )
        }
        </div>
      </Filters.Base>
    );
  }
}

export default Project;
