import React, { Component } from 'react';
import NumberFormat from 'react-number-format';
import { findIndex, map } from 'lodash';

import Filters from '../Filters';

class EventDate extends Component {

  constructor() {
    super();
    this.state = {
      select: 'Es',
      data: null,
      dia1: '',
      dia2: '',
      mes1: '',
      mes2: '',
      ano1: '',
      ano2: '',
    }

    this.filters = [];
    this.query = [];

    this.handleSelect = this.handleSelect.bind(this)
    this.addDate = this.addDate.bind(this)
    this.cleanFilters = this.cleanFilters.bind(this)
  }

  componentWillMount() {
    this.props.onRef(this)
    this.activeFilters(this.props.activeFilters)
  }

  handleSelect(e, input) {
    this.setState({ [input]: e.target.value })
  }

  handleInput(e, input) {
    this.setState({ [input]: e.value })
  }

  validForm() {
    const { mes1, ano1, mes2, ano2 } = this.state;
    const val1 = (mes1 !== '' && mes1 !== 'Mes') && (ano1 && ano1.length === 4);
    const val2 = (mes2 !== '' && mes2 !== 'Mes') && (ano2 && ano2.length === 4);

    return this.state.select === 'Entre' ? (val1 && val2) : (val1 || val2)
  }

  addDate() {
    const { dia1, mes1, ano1, dia2, mes2, ano2, select } = this.state;
    let dates, date

    switch (select) {
      case 'Entre':
        dates = [`${ano1}-${mes1}${(dia1 !== '') ? '-' + dia1 : ''}`, `${ano2}-${mes2}${(dia2 !== '') ? '-' + dia2 : ''}`]
        this.filters.push({ id: 'event_date=' + dates, label: select, idQ: dates, labelQ: 'event_date', value: dates })
        break;
      case 'Antes de finalizar':
        date = ['*', `${ano1}-${mes1}${(dia1 !== '') ? '-' + dia1 : ''}`]
        this.filters.push({ id: 'event_date=' + date, label: select, idQ: date, labelQ: 'event_date', value: date })
        break;
      case 'Después de comenzar':
        date = [`${ano1}-${mes1}${(dia1 !== '') ? '-' + dia1 : ''}`, '*']
        this.filters.push({ id: 'event_date=' + date, label: select, idQ: date, labelQ: 'event_date', value: date })
        break;
      default:
        date = [`${ano1}-${mes1}${(dia1 !== '') ? '-' + dia1 : ''}`]
        this.filters.push({ id: 'event_date=' + date, label: select, idQ: date, labelQ: 'event_date', value: date })
        break;
    }

    this.props.count(this.filters)
    this.setState({
      data: this.filters,
      select: 'Es',
      dia1: '',
      dia2: '',
      mes1: '',
      mes2: '',
      ano1: '',
      ano2: '',
    })
  }

  deleteFilter(value) {
    const item = findIndex(this.filters, value);
    this.filters.splice(item, 1);
    this.query.splice(item, 1);
    this.props.count(this.filters)
    this.setState({ data: this.filters })
  }

  cleanFilters() {
    this.filters = [];
    this.query = []
    this.props.count(this.filters)
    this.setState({ data: [] })
  }

  activeFilters(data) {
    map(data, (value, key) => {
      if (value.match(/event_date.*/)) {
        const filter = value.split('=');
        filter.shift();
        const parseFilter = filter.toString();
        let dia1, mes1, ano1, dia2, mes2, ano2, select;
        if (parseFilter.match(/^.*\*$/)) {
          select = 'Después de comenzar'
          let parseValue = (parseFilter.split(',')[1]).split('-')
          if(parseValue.length === 2) {
            dia1 = ''             
            mes1 = parseValue.pop()
            ano1 = parseValue.pop()
          } else {
            dia1 = parseValue.pop()
            mes1 = parseValue.pop()
            ano1 = parseValue.pop()
          }
        } else if (parseFilter.match(/^\*.*$/)) {
          select = 'Antes de finalizar'
          let parseValue = (parseFilter.split(',')[1]).split('-')
          if(parseValue.length === 2) {
            dia1 = ''            
            mes1 = parseValue.pop()
            ano1 = parseValue.pop()
          } else {
            dia1 = parseValue.pop()
            mes1 = parseValue.pop()
            ano1 = parseValue.pop()
          }
        } else {
          let sv;
          if (parseFilter.split(',').length === 1) {
            select = 'Es';
            sv = parseFilter.split('-')
            if(sv.length === 2) {
              dia1 = '' 
              mes1 = sv.pop()
              ano1 = sv.pop()
            } else {
              dia1 = sv.pop()
              mes1 = sv.pop()
              ano1 = sv.pop()
            }
          } else {
            select = 'Entre'
            let m = parseFilter.split(',')
            const date1 = m[0].split('-')
            if(date1.length === 2) {
              dia1 = ''               
              mes1 = date1.pop()
              ano1 = date1.pop()
            } else {
              dia1 = date1.pop()
              mes1 = date1.pop()
              ano1 = date1.pop()
            }
            
            const date2 = m[1].split('-')            
            if(date2.length === 2) {
              dia2 = ''               
              mes2 = date2.pop()
              ano2 = date2.pop()
            } else {
              dia2 = date2.pop()
              mes2 = date2.pop()
              ano2 = date2.pop()
            }
          }
        }

        this.setState({ dia1, mes1, ano1, dia2, mes2, ano2, select }, () => this.addDate())
      }
    })
  }

  render() {
    return (
      <Filters.Base title="Fecha del evento" handlerFilter={this.state.data} func={(value) => this.deleteFilter(value)}>
        <div>
          <div uk-form-custom="target: > * > span:first-child">
            <select className="uk-select uk-form-small" value={this.state.select} onChange={(e) => this.handleSelect(e, 'select')}>
              <option value="Es">Es</option>
              <option value="Antes de finalizar">Antes de finalizar</option>
              <option value="Después de comenzar">Después de comenzar</option>
              <option value="Entre">Entre</option>
            </select>
            <button className="uk-button uk-button-text" tabIndex="-1">
              <span>{this.state.select}</span>
              <span uk-icon="icon: triangle-down; ratio: 0.8"></span>
            </button>
          </div>
        </div>
        <form>
          <div className="uk-grid-small uk-child-width-expand" data-uk-grid="">
            <div className="uk-width-1-3">
              <NumberFormat className="uk-input uk-form-small" placeholder="Año" pattern="[0-9]{4}" value={this.state.ano1} onValueChange={(e) => this.handleInput(e, 'ano1')} />
            </div>
            <div>
              <select className="uk-select uk-form-small" value={this.state.mes1} onChange={(e) => this.handleSelect(e, 'mes1')}>
                <option>Mes</option>
                <option value="01">Enero</option>
                <option value="02">Febrero</option>
                <option value="03">Marzo</option>
                <option value="04">Abril</option>
                <option value="05">Mayo</option>
                <option value="06">Junio</option>
                <option value="07">Julio</option>
                <option value="08">Agosto</option>
                <option value="09">Septiembre</option>
                <option value="10">Octubre</option>
                <option value="11">Noviembre</option>
                <option value="12">Diciembre</option>
              </select>
            </div>
            <div className="uk-width-1-4">
              <NumberFormat className="uk-input uk-form-small" placeholder="Día" pattern="[0-9]{2}" value={this.state.dia1} onValueChange={(e) => this.handleInput(e, 'dia1')} />
            </div>
          </div>
          {this.state.select === 'Entre' && <div className="uk-text-center">Y</div>}
          {this.state.select === 'Entre' && <div className="uk-grid-small uk-child-width-expand" data-uk-grid="">
            <div className="uk-width-1-3">
              <NumberFormat className="uk-input uk-form-small" placeholder="Año" pattern="[0-9]{4}" onValueChange={(e) => this.handleInput(e, 'ano2')} />
            </div>
            <div>
              <select className="uk-select uk-form-small" value={this.state.mes2} onChange={(e) => this.handleSelect(e, 'mes2')}>
                <option>Mes</option>
                <option value="0">Enero</option>
                <option value="1">Febrero</option>
                <option value="2">Marzo</option>
                <option value="3">Abril</option>
                <option value="4">Mayo</option>
                <option value="5">Junio</option>
                <option value="6">Julio</option>
                <option value="7">Agosto</option>
                <option value="8">Septiembre</option>
                <option value="9">Octubre</option>
                <option value="10">Noviembre</option>
                <option value="11">Diciembre</option>
              </select>
            </div>
            <div className="uk-width-1-4">
              <NumberFormat className="uk-input uk-form-small" placeholder="Día" pattern="[0-9]{2}" onValueChange={(e) => this.handleInput(e, 'dia2')} />
            </div>
          </div>}
          <hr />
          <div className="uk-flex-between" data-uk-grid="">
            <div><button className="uk-button uk-button-default uk-button-small" onClick={this.cleanFilters}>Limpiar</button></div>
            {this.validForm() && <div><button className="uk-button uk-button-primary uk-button-small" onClick={this.addDate}>Añadir</button></div>}
          </div>
        </form>
      </Filters.Base>
    );
  }
}

export default EventDate;
