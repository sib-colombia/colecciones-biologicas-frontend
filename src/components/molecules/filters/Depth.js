import React, { Component } from 'react';
import { map, findKey, filter } from 'lodash';
import Filters from '../Filters';
import Range from '../../atoms/Range';
import Slider from '../../atoms/Slider';

class Depth extends Component {

  constructor() {
    super();
    this.state = {
      label: 'Sin Filtro',
      value: [0, 9999],
      data: null,
    }

    this.query = [];
    this.filters = [{ id: '', label: 'Sin Filtro', idQ: '', labelQ: 'depth', value: [0, 9999] }];

    this.handleSelect = this.handleSelect.bind(this);
    this.cleanFilters = this.cleanFilters.bind(this);
    this.createQuery = this.createQuery.bind(this);
    this.addRange = this.addRange.bind(this);
  }

  componentWillMount() {
    this.props.onRef(this)
  }

  changeValues = (value, key) => {
    this.filters[key].value = value;
    this.setState({ value });
  }

  handleSelect(e, key) {
    const defaultValues = {
      'Mínima': 0,
      'Máxima': 9999,
      'Es': 0
    }

    const filter = {
      label: e.target.value,
      value: (e.target.value === 'Sin Filtro' || e.target.value === 'Entre') ? [0, 9999] : (defaultValues[e.target.value])
    }

    this.filters[key] = filter;
    this.setState({ label: filter.label, value: filter.value })
  }

  addRange() {
    this.filters[this.filters.length - 1] = { id: 'depth=' + this.state.value, label: this.state.label, idQ: this.state.value, labelQ: 'depth', value: this.state.value }
    this.filters.push({ id: '', label: 'Sin Filtro', idQ: '', labelQ: 'depth', value: [0, 9999] })
    this.props.count(this.createQuery(this.filters))
    this.setState({ label: 'Sin Filtro', value: [0, 9999], data: this.filters })
  }

  deleteRange(key) {
    this.filters.splice(key, 1)
    this.query.splice(key, 1)
    this.props.count(this.createQuery(this.filters))
    this.props.count(this.filters)
    this.setState({ data: this.filters })
  }

  cleanFilters() {
    this.filters = [{ id: 'taxon_key=1', label: 'Sin Filtro', idQ: '1', labelQ: 'kingdom', value: [0, 9999] }];
    this.query = []
    this.props.count(this.createQuery(this.filters))
    this.setState({ data: this.query })
  }

  deleteFilter(value) {
    const position = findKey(this.filters, value);
    this.filters.splice(position, 1);
    this.query.splice(position, 1);
    this.props.count(this.createQuery(this.filters))
    this.setState({ data: this.filters })
  }

  createQuery(data) {
    let query = [];
    const filtro = filter(data, (o) => { return o.label !== 'Sin Filtro' });
    map(filtro, (value, key) => {
      query[key] = value
    })

    return query;
  }

  render() {
    return (
      <Filters.Base title="Profundidad" handlerFilter={this.createQuery(this.state.data)} func={(value) => this.deleteFilter(value)}>
        <ul className="uk-list uk-list-divider" >
          {
            this.filters && map(this.filters, (value, key) =>
              <li key={key}>
                <div className="uk-grid-collapse uk-flex-between uk-margin-small" data-uk-grid="">
                  <div uk-form-custom="target: > * > span:first-child">
                    <select className="uk-select" value={this.state.label} onChange={(e) => this.handleSelect(e, key)}>
                      <option value="Sin Filtro">Sin Filtro</option>
                      <option value="Es">Es</option>
                      <option value="Máxima">Máxima</option>
                      <option value="Mínima">Mínima</option>
                      <option value="Entre">Entre</option>
                    </select>
                    <button className="uk-button uk-button-text" tabIndex="-1">
                      <span>{value.label}</span>
                      <span uk-icon="icon: triangle-down; ratio: 0.8"></span>
                    </button>
                  </div>
                  {this.filters.length !== (key + 1) &&
                    <div>
                      <button type="button" data-uk-close onClick={() => this.deleteRange(key)}></button>
                    </div>
                  }
                </div>
                <div className="uk-flex-between uk-grid-collapse uk-margin-small" data-uk-grid="">
                  {(value.label === 'Es' || value.label === 'Entre' || value.label === 'Mínima') &&
                    <div className="uk-text-small">
                      {this.filters.length !== (key + 1) ?
                        (typeof value.value === 'object' ? value.value[0] : value.value)
                        :
                        (typeof this.state.value === 'object' ? this.state.value[0] : this.state.value)
                      }
                    </div>
                  }
                  {(value.label === 'Entre' || value.label === 'Máxima') &&
                    <div className="uk-text-small">
                      {this.filters.length !== (key + 1) ?
                        (typeof value.value === 'object' ? value.value[1] : value.value)
                        :
                        (typeof this.state.value === 'object' ? this.state.value[1] : this.state.value)
                      }
                    </div>
                  }
                </div>
                {value.label === 'Entre' &&
                  <Range
                    handleStyle={[
                      { backgroundColor: '#666', border: 0, width: 11, height: 11 },
                      { backgroundColor: '#666', border: 0, width: 11, height: 11 }
                    ]}
                    value={value.value}
                    values={(values) => this.changeValues(values, key)}
                    disabled={(value.label === 'Sin Filtro')}
                  />

                }
                {
                  value.label === 'Sin Filtro' &&
                  <Range
                    handleStyle={[
                      { backgroundColor: '#666', border: 0, width: 11, height: 11 },
                      { backgroundColor: '#666', border: 0, width: 11, height: 11 }
                    ]}
                    value={[0, 9999]}
                    disabled
                  />
                }
                {
                  (value.label !== 'Entre' && value.label !== 'Sin Filtro') &&
                  <Slider
                    handleStyle={[{ backgroundColor: '#666', border: 0, width: 11, height: 11 }]}
                    trackStyle={[value.label === 'Máxima' ? { backgroundColor: '#666', height: 1.5 } : { backgroundColor: '#e9e9e9', height: 1.5 }]}
                    railStyle={value.label === 'Mínima' ? { backgroundColor: '#666', height: 1.5 } : { backgroundColor: '#e9e9e9', height: 1.5 }}
                    values={(values) => this.changeValues(values, key)}
                    value={value.value}
                  />
                }
              </li>
            )
          }
        </ul>
        <div className={this.filters.length > 1 ? 'uk-flex uk-flex-between' : 'uk-flex uk-flex-right'} >
          {this.filters.length > 1 &&
            <div className="uk-width-auto">
              <button className="uk-button uk-button-default uk-button-small" onClick={this.cleanFilters}>Limpiar</button>
            </div>
          }
          {this.filters[this.filters.length - 1].label !== 'Sin Filtro' &&
            <div>
              <button className="uk-button uk-button-primary uk-button-small" onClick={this.addRange}>Añadir</button>
            </div>
          }
        </div>
      </Filters.Base>
    );
  }
}

export default Depth;
