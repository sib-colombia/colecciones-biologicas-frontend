import React, { Component } from 'react';

class Event extends Component {
  render() {
    const { data } = this.props;
    return (
      <ul className="uk-list uk-list-striped striped uk-margin-top">
        {
          (data.eventID && data.eventID !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">ID del evento</span>
            </div>
            <div>
              <span className="uk-text-break">{data.eventID}</span>
            </div>
          </li>
        }
        {
          (data.parentEventID && data.parentEventID !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">ID del evento parental</span>
            </div>
            <div>
              <span className="uk-text-break">{data.parentEventID}</span>
            </div>
          </li>
        }
        {
          (data.samplingProtocol && data.samplingProtocol !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Protocolo de muestreo</span>
            </div>
            <div>
              <span className="uk-text-break">{data.samplingProtocol}</span>
            </div>
          </li>
        }
        {
          (data.sampleSizeValue && data.sampleSizeValue !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Tamaño de la muestra</span>
            </div>
            <div>
              <span className="uk-text-break">{data.sampleSizeValue}</span>
            </div>
          </li>
        }
        {
          (data.sampleSizeUnit && data.sampleSizeUnit !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Unidad del tamaño</span>
            </div>
            <div>
              <span className="uk-text-break">{data.sampleSizeUnit}</span>
            </div>
          </li>
        }
        {
          (data.samplingEffort && data.samplingEffort !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Esfuerzo de muestreo</span>
            </div>
            <div>
              <span className="uk-text-break">{data.samplingEffort}</span>
            </div>
          </li>
        }
        {
          (data.eventDate && data.eventDate !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Fecha del evento</span>
            </div>
            <div>
              <span className="uk-text-break">{data.eventDate}</span>
            </div>
          </li>
        }
        {
          (data.eventTime && data.eventTime !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Hora del evento</span>
            </div>
            <div>
              <span className="uk-text-break">{data.eventTime}</span>
            </div>
          </li>
        }
        {
          (data.startDayOfYear && data.startDayOfYear !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Día inicial del año</span>
            </div>
            <div>
              <span className="uk-text-break">{data.startDayOfYear}</span>
            </div>
          </li>
        }
        {
          (data.endDayOfYear && data.endDayOfYear !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Día final del año</span>
            </div>
            <div>
              <span className="uk-text-break">{data.endDayOfYear}</span>
            </div>
          </li>
        }
        {
          (data.year && data.year !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Año</span>
            </div>
            <div>
              <span className="uk-text-break">{data.year}</span>
            </div>
          </li>
        }
        {
          (data.month && data.month !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Mes</span>
            </div>
            <div>
              <span className="uk-text-break">{data.month}</span>
            </div>
          </li>
        }
        {
          (data.day && data.day !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Día</span>
            </div>
            <div>
              <span className="uk-text-break">{data.day}</span>
            </div>
          </li>
        }
        {
          (data.verbatimEventDate && data.verbatimEventDate !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Fecha original del evento</span>
            </div>
            <div>
              <span className="uk-text-break">{data.verbatimEventDate}</span>
            </div>
          </li>
        }
        {
          (data.habitat && data.habitat !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Hábitat</span>
            </div>
            <div>
              <span className="uk-text-break">{data.habitat}</span>
            </div>
          </li>
        }
        {
          (data.fieldNumber && data.fieldNumber !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Número de campo</span>
            </div>
            <div>
              <span className="uk-text-break">{data.fieldNumber}</span>
            </div>
          </li>
        }
        {
          (data.fieldNotes && data.fieldNotes !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Notas de campo</span>
            </div>
            <div>
              <span className="uk-text-break">{data.fieldNotes}</span>
            </div>
          </li>
        }
        {
          (data.eventRemarks && data.eventRemarks !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Comentarios del evento</span>
            </div>
            <div>
              <span className="uk-text-break">{data.eventRemarks}</span>
            </div>
          </li>
        }
      </ul>
    )
  }
}

export default Event;
