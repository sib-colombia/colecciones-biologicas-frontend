import React, { Component } from 'react';

class Identification extends Component {
  render() {
    const { data } = this.props;
    return (
      <ul className="uk-list uk-list-striped striped uk-margin-top">
        {
          (data.identificationID && data.identificationID !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">ID de la identificación</span>
            </div>
            <div>
              <span className="uk-text-break">{data.identificationID}</span>
            </div>
          </li>
        }
        {
          (data.identifiedBy && data.identifiedBy !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Identificado por</span>
            </div>
            <div>
              <span className="uk-text-break">{data.identifiedBy}</span>
            </div>
          </li>
        }
        {
          (data.dateIdentified && data.dateIdentified !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Fecha de identificación</span>
            </div>
            <div>
              <span className="uk-text-break">{data.dateIdentified}</span>
            </div>
          </li>
        }
        {
          (data.identificationReferences && data.identificationReferences !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Referencias de la identificación</span>
            </div>
            <div>
              <span className="uk-text-break">{data.identificationReferences}</span>
            </div>
          </li>
        }
        {
          (data.identificationVerificationStatus && data.identificationVerificationStatus !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Estado de la verificación de la identificación</span>
            </div>
            <div>
              <span className="uk-text-break">{data.identificationVerificationStatus}</span>
            </div>
          </li>
        }
        {
          (data.identificationRemarks && data.identificationRemarks !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Comentarios de la Identificación</span>
            </div>
            <div>
              <span className="uk-text-break">{data.identificationRemarks}</span>
            </div>
          </li>
        }
        {
          (data.identificationQualifier && data.identificationQualifier !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Calificador de la identificación</span>
            </div>
            <div>
              <span className="uk-text-break">{data.identificationQualifier}</span>
            </div>
          </li>
        }
        {
          (data.typeStatus && data.typeStatus !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Estado del tipo</span>
            </div>
            <div>
              <span className="uk-text-break">{data.typeStatus}</span>
            </div>
          </li>
        }
      </ul>
    )
  }
}

export default Identification;
