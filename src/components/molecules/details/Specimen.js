import React, {Component} from 'react';

class Specimen extends Component {
  render() {
    const {data} = this.props;
    return (<div className="uk-grid-collapse uk-margin-small-top uk-padding-small uk-child-width-1-1 uk-striped" data-uk-grid="data-uk-grid">
      {
        (data.occurrenceID && data.occurrenceID !== '') && <div>
            <div className="uk-grid-small uk-child-width-1-2" data-uk-grid="data-uk-grid">
              <div>
                <span className="uk-text-bold">ID del registro biológico</span>
              </div>
              <div>
                <span className="uk-text-break">{data.occurrenceID}</span>
              </div>
            </div>
          </div>
      }
      {
        (data.catalogNumber && data.catalogNumber !== '') && <div>
            <div className="uk-grid-small uk-child-width-1-2" data-uk-grid="data-uk-grid">
              <div>
                <span className="uk-text-bold">Número de catálogo</span>
              </div>
              <div>
                <span>{data.catalogNumber}</span>
              </div>
            </div>
          </div>
      }
      {
        (data.occurrenceRemarks && data.occurrenceRemarks !== '') && <div>
            <div className="uk-grid-small uk-child-width-1-2" data-uk-grid="data-uk-grid">
              <div>
                <span className="uk-text-bold">Comentarios del registro biológico</span>
              </div>
              <div>
                <span>{data.occurrenceRemarks}</span>
              </div>
            </div>
          </div>
      }
      {
        (data.recordNumber && data.recordNumber !== '') && <div>
            <div className="uk-grid-small uk-child-width-1-2" data-uk-grid="data-uk-grid">
              <div>
                <span className="uk-text-bold">Número de registro</span>
              </div>
              <div>
                <span>{data.recordNumber}</span>
              </div>
            </div>
          </div>
      }
      {
        (data.recordedBy && data.recordedBy !== '') && <div>
            <div className="uk-grid-small uk-child-width-1-2" data-uk-grid="data-uk-grid">
              <div>
                <span className="uk-text-bold">Registrado por</span>
              </div>
              <div>
                <span>{data.recordedBy}</span>
              </div>
            </div>
          </div>
      }
      {
        (data.organismID && data.organismID !== '') && <div>
            <div className="uk-grid-small uk-child-width-1-2" data-uk-grid="data-uk-grid">
              <div>
                <span className="uk-text-bold">ID del individuo</span>
              </div>
              <div>
                <span>{data.organismID}</span>
              </div>
            </div>
          </div>
      }
      {
        (data.individualCount && data.individualCount !== undefined) && <div>
            <div className="uk-grid-small uk-child-width-1-2" data-uk-grid="data-uk-grid">
              <div>
                <span className="uk-text-bold">Número de individuos</span>
              </div>
              <div>
                <span>{data.individualCount}</span>
              </div>
            </div>
          </div>
      }
      {
        (data.sex && data.sex !== '') && <div>
            <div className="uk-grid-small uk-child-width-1-2" data-uk-grid="data-uk-grid">
              <div>
                <span className="uk-text-bold">Sexo</span>
              </div>
              <div>
                <span>{data.sex}</span>
              </div>
            </div>
          </div>
      }
      {
        (data.lifeStage && data.lifeStage !== '') && <div>
            <div className="uk-grid-small uk-child-width-1-2" data-uk-grid="data-uk-grid">
              <div>
                <span className="uk-text-bold">Etapa de vida</span>
              </div>
              <div>
                <span>{data.lifeStage}</span>
              </div>
            </div>
          </div>
      }
      {
        (data.reproductiveCondition && data.reproductiveCondition !== '') && <div>
            <div className="uk-grid-small uk-child-width-1-2" data-uk-grid="data-uk-grid">
              <div>
                <span className="uk-text-bold">Condición reproductiva</span>
              </div>
              <div>
                <span>{data.reproductiveCondition}</span>
              </div>
            </div>
          </div>
      }
      {
        (data.behavior && data.behavior !== '') && <div>
            <div className="uk-grid-small uk-child-width-1-2" data-uk-grid="data-uk-grid">
              <div>
                <span className="uk-text-bold">Comportamiento</span>
              </div>
              <div>
                <span>{data.behavior}</span>
              </div>
            </div>
          </div>
      }
      {
        (data.establishmentMeans && data.establishmentMeans !== '') && <div>
            <div className="uk-grid-small uk-child-width-1-2" data-uk-grid="data-uk-grid">
              <div>
                <span className="uk-text-bold">Medios de establecimiento</span>
              </div>
              <div>
                <span>{data.establishmentMeans}</span>
              </div>
            </div>
          </div>
      }
      {
        (data.occurrenceStatus && data.occurrenceStatus !== '') && <div>
            <div className="uk-grid-small uk-child-width-1-2" data-uk-grid="data-uk-grid">
              <div>
                <span className="uk-text-bold">Estado del registro biológico</span>
              </div>
              <div>
                <span>{data.occurrenceStatus}</span>
              </div>
            </div>
          </div>
      }
      {
        (data.preparations && data.preparations !== '') && <div>
            <div className="uk-grid-small uk-child-width-1-2" data-uk-grid="data-uk-grid">
              <div>
                <span className="uk-text-bold">Preparaciones</span>
              </div>
              <div>
                <span>{data.preparations}</span>
              </div>
            </div>
          </div>
      }
      {
        (data.disposition && data.disposition !== '') && <div>
            <div className="uk-grid-small uk-child-width-1-2" data-uk-grid="data-uk-grid">
              <div>
                <span className="uk-text-bold">Disposición</span>
              </div>
              <div>
                <span>{data.disposition}</span>
              </div>
            </div>
          </div>
      }
      {
        (data.otherCatalogNumbers && data.otherCatalogNumbers !== '') && <div>
            <div className="uk-grid-small uk-child-width-1-2" data-uk-grid="data-uk-grid">
              <div>
                <span className="uk-text-bold">Otros números de catálogo</span>
              </div>
              <div>
                <span>{data.otherCatalogNumbers}</span>
              </div>
            </div>
          </div>
      }
      {
        (data.associatedMedia && data.associatedMedia !== '') && <div>
            <div className="uk-grid-small uk-child-width-1-2" data-uk-grid="data-uk-grid">
              <div>
                <span className="uk-text-bold">Medios asociados</span>
              </div>
              <div>
                <span>{data.associatedMedia}</span>
              </div>
            </div>
          </div>
      }
      {
        (data.associatedReferences && data.associatedReferences !== '') && <div>
            <div className="uk-grid-small uk-child-width-1-2" data-uk-grid="data-uk-grid">
              <div>
                <span className="uk-text-bold">Referencias asociadas</span>
              </div>
              <div>
                <span>{data.associatedReferences}</span>
              </div>
            </div>
          </div>
      }
      {
        (data.associatedSequences && data.associatedSequences !== '') && <div>
            <div className="uk-grid-small uk-child-width-1-2" data-uk-grid="data-uk-grid">
              <div>
                <span className="uk-text-bold">Secuencias asociadas</span>
              </div>
              <div>
                <span>{data.associatedSequences}</span>
              </div>
            </div>
          </div>
      }
      {
        (data.associatedTaxa && data.associatedTaxa !== '') && <div>
            <div className="uk-grid-small uk-child-width-1-2" data-uk-grid="data-uk-grid">
              <div>
                <span className="uk-text-bold">Taxones asociados</span>
              </div>
              <div>
                <span>{data.associatedTaxa}</span>
              </div>
            </div>
          </div>
      }
    </div>);
  }
}

export default Specimen;
