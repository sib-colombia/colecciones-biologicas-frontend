import React, { Component } from 'react';

import License from '../../atoms/License';


class Occurrence extends Component {
  render() {
    const { data } = this.props;
    return (
      <ul className="uk-list uk-list-striped striped uk-margin-top">
        {
          (data.basisOfRecord && data.basisOfRecord !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Base del registro</span>
            </div>
            <div>
              <span className="uk-text-break">{data.basisOfRecord}</span>
            </div>
          </li>
        }
        {
          (data.institutionCode && data.institutionCode !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Código de la institución</span>
            </div>
            <div>
              <span className="uk-text-break">{data.institutionCode}</span>
            </div>
          </li>
        }
        {
          (data.collectionCode && data.collectionCode !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Código de la colección</span>
            </div>
            <div>
              <span className="uk-text-break">{data.collectionCode}</span>
            </div>
          </li>
        }
        {
          (data.type && data.type !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Tipo</span>
            </div>
            <div>
              <span className="uk-text-break">{data.type}</span>
            </div>
          </li>
        }
        {
          (data.modified && data.modified !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Modificado</span>
            </div>
            <div>
              <span className="uk-text-break">{data.modified}</span>
            </div>
          </li>
        }
        {
          (data.language && data.language !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Idioma</span>
            </div>
            <div>
              <span className="uk-text-break">{data.language}</span>
            </div>
          </li>
        }
        {
          (data.license && data.license !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Licencia</span>
            </div>
            <div>
              <span className="uk-text-break"><License id={data.license} /></span>
            </div>
          </li>
        }
        {
          (data.rightsHolder && data.rightsHolder !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Titular de los derechos</span>
            </div>
            <div>
              <span className="uk-text-break">{data.rightsHolder}</span>
            </div>
          </li>
        }
        {
          (data.accessRights && data.accessRights !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Derechos de acceso</span>
            </div>
            <div>
              <span className="uk-text-break">{data.accessRights}</span>
            </div>
          </li>
        }
        {
          (data.bibliographicCitation && data.bibliographicCitation !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Citación bibliográfica</span>
            </div>
            <div>
              <span className="uk-text-break">{data.bibliographicCitation}</span>
            </div>
          </li>
        }
        {
          (data.references && data.references !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Referencias</span>
            </div>
            <div>
              <span className="uk-text-break">{data.references}</span>
            </div>
          </li>
        }
        {
          (data.institutionID && data.institutionID !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">ID de la institución</span>
            </div>
            <div>
              <span className="uk-text-break">{data.institutionID}</span>
            </div>
          </li>
        }
        {
          (data.collectionID && data.collectionID !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">ID de la colección</span>
            </div>
            <div>
              <span className="uk-text-break">{data.collectionID}</span>
            </div>
          </li>
        }
        {
          (data.datasetID && data.datasetID !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">ID del conjunto de datos</span>
            </div>
            <div>
              <span className="uk-text-break">{data.datasetID}</span>
            </div>
          </li>
        }
        {
          (data.datasetName && data.datasetName !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Nombre del conjunto de datos</span>
            </div>
            <div>
              <span className="uk-text-break">{data.datasetName}</span>
            </div>
          </li>
        }
        {
          (data.ownerInstitutionCode && data.ownerInstitutionCode !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Código de la institución propietaria</span>
            </div>
            <div>
              <span className="uk-text-break">{data.ownerInstitutionCode}</span>
            </div>
          </li>
        }
        {
          (data.informationWithheld && data.informationWithheld !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Información retenida</span>
            </div>
            <div>
              <span className="uk-text-break">{data.informationWithheld}</span>
            </div>
          </li>
        }
        {
          (data.dataGeneralizations && data.dataGeneralizations !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Generalización de los datos</span>
            </div>
            <div>
              <span className="uk-text-break">{data.dataGeneralizations}</span>
            </div>
          </li>
        }
        {
          (data.dynamicProperties && data.dynamicProperties !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Propiedades dinámicas</span>
            </div>
            <div>
              <span className="uk-text-break">{data.dynamicProperties}</span>
            </div>
          </li>
        }
      </ul>
    );
  }
}

export default Occurrence;
