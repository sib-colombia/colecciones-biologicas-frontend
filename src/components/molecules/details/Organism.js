import React, { Component } from 'react';

class Organism extends Component {
  render() {
    const { data } = this.props;
    return (
      <ul className="uk-list uk-list-striped striped uk-margin-top">
        {
          (data.organismQuantity && data.organismQuantity !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Cantidad del Organismo</span>
            </div>
            <div>
              <span className="uk-text-break">{data.organismQuantity}</span>
            </div>
          </li>
        }
        {
          (data.organismQuantityType && data.organismQuantityType !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Tipo de Cantidad del Organismo</span>
            </div>
            <div>
              <span className="uk-text-break">{data.organismQuantityType}</span>
            </div>
          </li>
        }
        {
          (data.organismName && data.organismName !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Nombre del organismo</span>
            </div>
            <div>
              <span className="uk-text-break">{data.organismName}</span>
            </div>
          </li>
        }
        {
          (data.organismScope && data.organismScope !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Alcance del organismo</span>
            </div>
            <div>
              <span className="uk-text-break">{data.organismScope}</span>
            </div>
          </li>
        }
        {
          (data.associatedOrganisms && data.associatedOrganisms !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Organismos asociados</span>
            </div>
            <div>
              <span className="uk-text-break">{data.associatedOrganisms}</span>
            </div>
          </li>
        }
        {
          (data.organismRemarks && data.organismRemarks !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Comentarios del organismo</span>
            </div>
            <div>
              <span className="uk-text-break">{data.organismRemarks}</span>
            </div>
          </li>
        }
        {
          (data.previousIdentifications && data.previousIdentifications !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Identificaciones previas</span>
            </div>
            <div>
              <span className="uk-text-break">{data.previousIdentifications}</span>
            </div>
          </li>
        }
        {
          (data.associatedOccurrences && data.associatedOccurrences !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Registros biológicos asociados</span>
            </div>
            <div>
              <span className="uk-text-break">{data.associatedOccurrences}</span>
            </div>
          </li>
        }
      </ul>
    )
  }
}

export default Organism;
