import React, { Component } from 'react';

class Registro extends Component {
  render() {
    const { data } = this.props;
    return (
      <ul className="uk-list uk-list-striped striped uk-margin-top">
        {
          (data.occurrenceID && data.occurrenceID !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">ID del registro biológico</span>
            </div>
            <div>
              <span className="uk-text-break">{data.occurrenceID}</span>
            </div>
          </li>
        }
        {
          (data.catalogNumber && data.catalogNumber !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Número de catálogo</span>
            </div>
            <div>
              <span className="uk-text-break">{data.catalogNumber}</span>
            </div>
          </li>
        }
        {
          (data.occurrenceRemarks && data.occurrenceRemarks !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Comentarios del registro biológico</span>
            </div>
            <div>
              <span className="uk-text-break">{data.occurrenceRemarks}</span>
            </div>
          </li>
        }
        {
          (data.recordNumber && data.recordNumber !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Número de registro</span>
            </div>
            <div>
              <span className="uk-text-break">{data.recordNumber}</span>
            </div>
          </li>
        }
        {
          (data.recordedBy && data.recordedBy !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Registrado por</span>
            </div>
            <div>
              <span className="uk-text-break">{data.recordedBy}</span>
            </div>
          </li>
        }
        {
          (data.organismID && data.organismID !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">ID del individuo</span>
            </div>
            <div>
              <span className="uk-text-break">{data.organismID}</span>
            </div>
          </li>
        }
        {
          (data.individualCount && data.individualCount !== undefined) &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Número de individuos</span>
            </div>
            <div>
              <span className="uk-text-break">{data.individualCount}</span>
            </div>
          </li>
        }
        {
          (data.sex && data.sex !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Sexo</span>
            </div>
            <div>
              <span className="uk-text-break">{data.sex}</span>
            </div>
          </li>
        }
        {
          (data.lifeStage && data.lifeStage !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Etapa de vida</span>
            </div>
            <div>
              <span className="uk-text-break">{data.lifeStage}</span>
            </div>
          </li>
        }
        {
          (data.reproductiveCondition && data.reproductiveCondition !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Condición reproductiva</span>
            </div>
            <div>
              <span className="uk-text-break">{data.reproductiveCondition}</span>
            </div>
          </li>
        }
        {
          (data.behavior && data.behavior !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Comportamiento</span>
            </div>
            <div>
              <span className="uk-text-break">{data.behavior}</span>
            </div>
          </li>
        }
        {
          (data.establishmentMeans && data.establishmentMeans !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Medios de establecimiento</span>
            </div>
            <div>
              <span className="uk-text-break">{data.establishmentMeans}</span>
            </div>
          </li>
        }
        {
          (data.occurrenceStatus && data.occurrenceStatus !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Estado del registro biológico</span>
            </div>
            <div>
              <span className="uk-text-break">{data.occurrenceStatus}</span>
            </div>
          </li>
        }
        {
          (data.preparations && data.preparations !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Preparaciones</span>
            </div>
            <div>
              <span className="uk-text-break">{data.preparations}</span>
            </div>
          </li>
        }
        {
          (data.disposition && data.disposition !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Disposición</span>
            </div>
            <div>
              <span className="uk-text-break">{data.disposition}</span>
            </div>
          </li>
        }
        {
          (data.otherCatalogNumbers && data.otherCatalogNumbers !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Otros números de catálogo</span>
            </div>
            <div>
              <span className="uk-text-break">{data.otherCatalogNumbers}</span>
            </div>
          </li>
        }
        {
          (data.associatedMedia && data.associatedMedia !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Medios asociados</span>
            </div>
            <div>
              <span className="uk-text-break">{data.associatedMedia}</span>
            </div>
          </li>
        }
        {
          (data.associatedReferences && data.associatedReferences !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Referencias asociadas</span>
            </div>
            <div>
              <span className="uk-text-break">{data.associatedReferences}</span>
            </div>
          </li>
        }
        {
          (data.associatedSequences && data.associatedSequences !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Secuencias asociadas</span>
            </div>
            <div>
              <span className="uk-text-break">{data.associatedSequences}</span>
            </div>
          </li>
        }
        {
          (data.associatedTaxa && data.associatedTaxa !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Taxones asociados</span>
            </div>
            <div>
              <span className="uk-text-break">{data.associatedTaxa}</span>
            </div>
          </li>
        }
      </ul>
    )
  }
}

export default Registro;
