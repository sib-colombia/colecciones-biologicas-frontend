import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import * as OccurrenceService from '../../services/OccurrenceService';

class OccurrenceRow extends Component {

  constructor(props) {
    super(props);
    this.state = {
      url: false
    }
  }
  
  ubicacion(r) {
    var t = ""
    if (r.decimalLatitude) {
      if (r.decimalLatitude > 0)
        t += r.decimalLatitude.toFixed(2) + " N"
      else
        t += (-r.decimalLatitude).toFixed(2) + " S"
    }
    if (r.decimalLongitude) {
      t += ", "
      if (r.decimalLongitude > 0)
        t += r.decimalLongitude.toFixed(2) + " E"
      else
        t += (-r.decimalLongitude).toFixed(2) + " W"
    }
    return t
  }

  fecha(f) {
    if (f.eventDate) {
      let fecha = new Date(f.eventDate);
      return fecha.toISOString().substr(0, 10)
    }
    return ""
  }

  capitalize(d) {
    const string = d.charAt(0).toUpperCase() + (d.slice(1)).toLowerCase();
    return string;
  }
  
  imagen(d){
    if(d.media_type && d.media_type==="StillImage"){
      return <Link className="uk-link-reset" to={`/occurrence/${d.id}`}>
        <img src={OccurrenceService.getThumb(d.media_url)} alt="" />
      </Link>
    }else{
      return <Link className="uk-link-reset" to={`/occurrence/${d.id}`}>
        <img src="/static/css/images/foto_especimen.png" alt="" />
      </Link>
    }
  }
  
  componentWillMount() {
    this.imagen(this.props.data._source)
  }
  imagen(d){
    if(d.mediaType && d.mediaType==="StillImage"){
      OccurrenceService.getThumbID(d).then(data => {
        console.log("Consultando imágen: ", data, OccurrenceService.getThumb(data.url))
        this.setState({"url": OccurrenceService.getThumb(data.url)})
      });
    }
  }

  render() {
    let { data } = this.props;
    data = data._source;
    if(data.id!==""){
      data.id = data.id;
    }
    
    return (<tr>
      <td className="uk-width-1-4" >
          { this.state.url && <img src={this.state.url} alt="Imágen" /> }
          { !this.state.url && <img src='/static/css/images/foto_especimen.png' alt="Imágen" /> }
      </td>
      <td>
        <Link className="uk-link-reset scientificName" to={`/occurrence/${data.id}`}>{data.scientificName}</Link>
      </td>
      <td>
        <Link className="uk-link-reset" to={`/occurrence/${data.id}`}>{data.catalogNumber}</Link>
      </td>
      <td>
        <Link className="uk-link-reset" to={`/occurrence/${data.id}`}>{data.collectionCode}</Link>
      </td>
      <td className="uk-text-truncate">
        <Link className="uk-link-reset" to={`/occurrence/${data.id}`}>{data.institutionCode}</Link>
      </td>
    </tr>);
  }
}

export default OccurrenceRow;
