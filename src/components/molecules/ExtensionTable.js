import React, { Component } from 'react';
import { union, indexOf, fill, map } from 'lodash';

class ExtensionTable extends Component {


  render() {
    const { titulo, data } = this.props;
    
    if(data===undefined || data===null) return <div/>;
    
    let traduccion = {
"acceptedNameUsageID": "ID del nombre aceptado usado", 
"acceptedNameUsage": "Nombre aceptado usado", 
"accessRights": "Derechos de acceso", 
"ageInDays": "Edad en días", 
"appendixCITES": "Apéndices CITES", 
"associatedMedia": "Medios asociados", 
"associatedOccurrences": "Registros biológicos asociados", 
"associatedOrganisms": "Organismos asociados", 
"associatedReferences": "Referencias asociadas", 
"associatedSequences": "Secuencias asociadas", 
"associatedTaxa": "Taxones asociados", 
"audience": "Audiencia", 
"basisOfRecord": "Base del registro", 
"bed": "Capa", 
"behavior": "Comportamiento", 
"bibliographicCitation": "Citación bibliográfica", 
"catalogNumber": "Número de catálogo", 
"class": "Clase", 
"collectionCode": "Código de la colección", 
"collectionID": "ID de la colección", 
"continent": "Continente", 
"contributor": "Parte asociada", 
"coordinatePrecision": "Precisión de las coordenadas", 
"coordinateUncertaintyInMeters": "Incertidumbre de las coordenadas en metros", 
"countryCode": "Código del país", 
"country": "País", 
"county": "Municipio", 
"created": "Fecha de creación", 
"creator": "Autor", 
"dataGeneralizations": "Generalización de los datos", 
"datasetID": "ID del conjunto de datos", 
"datasetName": "Nombre del conjunto de datos", 
"date": "Fecha", 
"dateIdentified": "Fecha de identificación", 
"day": "Día", 
"decimalLatitude": "Latitud decimal", 
"decimalLongitude": "Longitud decimal", 
"description": "Descripción", 
"disposition": "Disposición", 
"dynamicProperties": "Propiedades dinámicas", 
"earliestAgeOrLowestStage": "Edad temprana o piso inferior", 
"earliestEonOrLowestEonothem": "Eón temprano o eonotema inferior", 
"earliestEpochOrLowestSeries": "Época temprana o serie inferior", 
"earliestEraOrLowestErathem": "Era temprana o eratema inferior", 
"earliestPeriodOrLowestSystem": "Periodo temprano o sistema inferior", 
"endDayOfYear": "Día final del año", 
"establishmentMeans": "Medios de establecimiento", 
"eventDate": "Fecha del evento", 
"eventID": "ID del evento", 
"eventRemarks": "Comentarios del evento", 
"eventTime": "Hora del evento", 
"family": "Familia", 
"fieldNotes": "Notas de campo", 
"fieldNumber": "Número de campo", 
"footprintSpatialFit": "Ajuste espacial de footprint", 
"footprintSRS": "SRS footprint", 
"footprintWKT": "WKT footprint", 
"format": "Formato", 
"formation": "Formación", 
"genus": "Género", 
"geodeticDatum": "Datum geodésico", 
"geologicalContextID": "ID del contexto geológico", 
"georeferencedBy": "Georreferenciado por", 
"georeferencedDate": "Fecha de georreferenciación", 
"georeferenceProtocol": "Protocolo de georreferenciación", 
"georeferenceRemarks": "Comentarios de la georreferenciación", 
"georeferenceSources": "Fuentes de georreferenciación", 
"georeferenceVerificationStatus": "Estado de la verificación de la georreferenciación", 
"group": "Grupo", 
"habitat": "Hábitat", 
"higherClassification": "Clasificación superior", 
"higherGeography": "Geografía superior", 
"higherGeographyID": "ID de la geografía superior", 
"highestBiostratigraphicZone": "Zona bioestratigráfica superior", 
"identificationID": "ID de la identificación", 
"identificationQualifier": "Calificador de la identificación", 
"identificationReferences": "Referencias de la identificación", 
"identificationRemarks": "Comentarios de la Identificación", 
"identificationVerificationStatus": "Estado de la verificación de la identificación", 
"identifiedBy": "Identificado por", 
"identifier": "Identificador", 
"id": "ID del registro biológico", 
"individualCount": "Número de individuos", 
"informationWithheld": "Información retenida", 
"infraspecificEpithet": "Epíteto infraespecífico", 
"institutionCode": "Código de la institución", 
"institutionID": "ID de la institución", 
"isExtinct": "Extinto", 
"isFreshwater": "Es de agua dulce", 
"isHybrid": "Es híbrido", 
"isInvasive": "Es Invasor", 
"islandGroup": "Grupo de islas", 
"island": "Isla", 
"isMarine": "Es marino", 
"isPlural": "Nombre en plural", 
"isPreferredName": "Nombre preferido", 
"isTerrestrial": "Es terrestre", 
"kingdom": "Reino", 
"language": "Idioma", 
"latestAgeOrHighestStage": "Edad tardía o piso superior", 
"latestEonOrHighestEonothem": "Eón tardío o eonotema superior", 
"latestEpochOrHighestSeries": "Época tardía o serie superior", 
"latestEraOrHighestErathem": "Era tardía o eratema superior", 
"latestPeriodOrHighestSystem": "Periodo tardío o sistema superior", 
"license": "Licencia", 
"lifeForm": "Forma de vida", 
"lifeStage": "Etapa de vida", 
"lithostratigraphicTerms": "Términos litoestratigráficos", 
"livingPeriod": "Periodo de vida", 
"locality": "Localidad", 
"locationAccordingTo": "Ubicación de acuerdo con", 
"locationID": "ID de la ubicación", 
"locationRemarks": "Comentarios de la ubicación", 
"lowestBiostratigraphicZone": "Zona bioestratigráfica inferior", 
"massInGrams": "Peso en gramos", 
"materialSampleID": "ID de muestra del ejemplar", 
"maximumDepthInMeters": "Profundidad máxima en metros", 
"maximumDistanceAboveSurfaceInMeters": "Distancia máxima de la superficie metros", 
"maximumElevationInMeters": "Elevación máxima en metros", 
"measurementAccuracy": "Precisión", 
"measurementDeterminedBy": "Determinado por", 
"measurementDeterminedDate": "Fecha", 
"measurementID": "Identificador", 
"measurementMethod": "Método", 
"measurementRemarks": "Comentarios", 
"measurementType": "Tipo", 
"measurementUnit": "Unidad de medida", 
"measurementValue": "Valor", 
"member": "Miembro", 
"minimumDepthInMeters": "Profundidad mínima en metros", 
"minimumDistanceAboveSurfaceInMeters": "Distancia mínima de la superficie metros", 
"minimumElevationInMeters": "Elevación mínima en metros", 
"modified": "Modificado", 
"month": "Mes", 
"municipality": "Centro poblado / Cabecera municipal", 
"nameAccordingToID": "ID del nombre de acuerdo con", 
"nameAccordingTo": "Nombre de acuerdo con", 
"namePublishedInID": "ID del nombre publicado en", 
"namePublishedIn": "Nombre publicado en", 
"namePublishedInYear": "Nombre publicado en el año", 
"nomenclaturalCode": "Código nomenclatural", 
"nomenclaturalStatus": "Estado nomenclatural", 
"occurrenceID": "ID del registro biológico", 
"occurrenceRemarks": "Comentarios del registro biológico", 
"occurrenceStatus": "Estado del registro biológico", 
"order": "Orden", 
"organismID": "ID del individuo", 
"organismName": "Nombre del organismo", 
"organismPart": "Parte del organismo", 
"organismQuantity": "Cantidad del Organismo", 
"organismQuantityType": "Tipo de Cantidad del Organismo", 
"organismRemarks": "Comentarios del organismo", 
"organismScope": "Alcance del organismo", 
"originalNameUsageID": "ID del nombre original usado", 
"originalNameUsage": "Nombre original usado", 
"otherCatalogNumbers": "Otros números de catálogo", 
"ownerInstitutionCode": "Código de la institución propietaria", 
"parentEventID": "ID del evento parental", 
"parentNameUsageID": "ID del nombre parental usado", 
"parentNameUsage": "Nombre parental usado", 
"phylum": "Filo", 
"pointRadiusSpatialFit": "Ajuste espacial del radio-punto", 
"preparations": "Preparaciones", 
"previousIdentifications": "Identificaciones previas", 
"publisher": "Editor", 
"recordedBy": "Registrado por", 
"recordNumber": "Número de registro", 
"references": "Referencias", 
"relatedResourceID": "ID del recurso relacionado", 
"relationshipAccordingTo": "Relación de acuerdo a", 
"relationshipEstablishedDate": "Fecha de relación", 
"relationshipOfResource": "Relación de recursos", 
"relationshipRemarks": "Comentarios",
"reproductiveCondition": "Condición reproductiva", 
"resourceID ": "ID del recurso", 
"resourceRelationshipID": "ID de relación", 
"rights": "Derechos de autor", 
"rightsHolder": "Titular de los derechos", 
"sampleSizeUnit": "Unidad del tamaño", 
"sampleSizeValue": "Tamaño de la muestra", 
"samplingEffort": "Esfuerzo de muestreo", 
"samplingProtocol": "Protocolo de muestreo", 
"scientificNameAuthorship": "Autoría del nombre científico", 
"scientificNameID": "ID del nombre científico", 
"scientificName": "Nombre científico", 
"sex": "Sexo", 
"sizeInMillimeters": "Tamaño en milímetros", 
"source": "Fuente", 
"specificEpithet": "Epíteto específico", 
"startDayOfYear": "Día inicial del año", 
"stateProvince": "Departamento", 
"subgenus": "Subgénero", 
"subject": "Palabras clave", 
"taxonConceptID": "ID del concepto del taxón", 
"taxon": "ID del taxón", 
"taxonomicStatus": "Estado taxonómico", 
"taxonRank": "Categoría del taxón", 
"taxonRemarks": "Comentarios del taxón", 
"temporal": "Contexto temporal", 
"threatStatus": "Estado de amenaza", 
"title": "Título", 
"typeDesignatedBy": "Tipo designado por", 
"typeDesignationType": "Designación del Tipo", 
"typeStatus": "Estado del Tipo", 
"type": "Tipo", 
"verbatimCoordinates": "Coordenadas originales", 
"verbatimCoordinateSystem": "Sistema original de coordenadas", 
"verbatimDepth": "Profundidad original", 
"verbatimElevation": "Elevación original", 
"verbatimEventDate": "Fecha original del evento", 
"verbatimLabel": "Etiqueta original", 
"verbatimLatitude": "Latitud original", 
"verbatimLocality": "Localidad original", 
"verbatimLongitude": "Longitud original", 
"verbatimSRS": "SRS original", 
"verbatimTaxonRank": "Categoría original del taxón", 
"vernacularName": "Nombre común", 
"waterBody": "Cuerpo de agua", 
"year": "Año"
}
    let header;
    let body = [];
    map(data, (value, key) => {
      let ks = Object.keys(value)
      if (ks.indexOf("hasLocation")!==-1)
        ks.splice(ks.indexOf("hasLocation"), 1);
      if (ks.indexOf("id")!==-1)
        ks.splice(ks.indexOf("id"), 1);
      header = union(header, ks)
      body.push(fill(Array(header.length), ''))
      map(value, (v, k) => {
        body[key][indexOf(header, k)] = v;
      })
    })
  
    return (
      <div>
        <h3>{titulo}</h3>

        <div className="uk-overflow-auto uk-margin-top">
          <table className="uk-card uk-card-default uk-table uk-table-divider uk-table-small">
            <thead>
              <tr>
                {
                  map(header, (title, key) => (
                    <th key={key} className="uk-width-small">{traduccion[title]}</th>
                  ))
                }
              </tr>
            </thead>
            <tbody>
              {
                map(body, (value, key) => (
                  <tr key={key}>
                    {
                      map(value, (v, k) => 
                        {
                        if(typeof v === 'string')
                          return <td key={k} className="uk-text-break">
                            {
                              v && typeof v === 'string' &&
                              v.match(/^http.*$/) ? <a href={v} target="_blank">{v}</a> : v
                            }
                          </td>
                        else
                          return <td key={k} className="uk-text-break"></td>
                        }
                      )
                    }
                  </tr>
                ))
              }
            </tbody>
          </table>
        </div>
      </div>
    )
  }
}

export default ExtensionTable;
