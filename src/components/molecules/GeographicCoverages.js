import React, {Component} from 'react';
import {Rectangle} from 'react-leaflet';

import map from 'lodash/map';

class GeographicCoverages extends Component {

  constructor(props) {
    super(props);
    this.state = {
    }
  }

  render() {
    return (
      <div>
        { map(this.props.g,(r, key)  => {
          return <Rectangle key={key} bounds={[
            [r.boundingBox.minLatitude, r.boundingBox.minLongitude],
            [r.boundingBox.maxLatitude, r.boundingBox.maxLongitude]
          ]}/>
        })}
      </div>
    );
  }
}

export default GeographicCoverages;
