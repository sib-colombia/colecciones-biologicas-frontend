import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class ProvidersGridItem extends Component {
  render() {
    const { data } = this.props;
    let x = data.platform.hits.hits[0]._source
    return (
      <div>
        <div className="uk-card uk-card-default uk-child-width-1-1 uk-grid-collapse" data-uk-grid>
          <div className="uk-flex uk-flex-center uk-heading-divider">
            <div><Link to={`/provider/${data.key}`}><img style={{"max-height": "231px"}}src={x.resourceLogoUrl} alt="" /></Link></div>
          </div>
          <h5 className="uk-text-tertiary uk-text-truncate uk-text-bold uk-margin-small-left uk-margin-small-right uk-margin-small-top">
            <Link className="uk-link-reset" to={`/provider/${data.key}`}>{x.organizationName}</Link>
          </h5>
          <div className="uk-background-tertiary uk-text-default uk-text-small uk-maximo-20">
            <Link className="uk-link-reset" to={`/provider/${data.key}`}><span className="uk-text-bold uk-margin-small-left">{data.doc_count}</span> REGISTROS</Link>
          </div>
        </div>
      </div>
    )
  }
}

export default ProvidersGridItem;
