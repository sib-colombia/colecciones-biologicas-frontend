import React, { Component } from 'react';
import { Link } from 'react-router-dom';

export class GalleryItem extends Component {
  render() {
    // const { data } = this.props;

    return (
      <div>
        <div className="uk-card uk-card-default uk-child-width-1-1 uk-grid-collapse" data-uk-grid>
          <div className="uk-flex uk-flex-center" >
            <Link className="uk-link-reset" to={`/collection/id`}>
              <img src="/static/css/images/foto_especimen.png" alt="" />
            </Link>
          </div>
          <h5 className="uk-text-tertiary uk-text-truncate uk-text-bold uk-margin-small-left uk-margin-small-right uk-margin-small-top">
            <Link className="uk-link-reset" to={`/collection/id`}><i>Nombre de la especie</i></Link>
          </h5>
          <h6 className="uk-margin-small-left uk-margin-small-bottom">
            <Link className="uk-link-reset uk-text-small" to={`/provider/id`}>Colección</Link>
          </h6>
        </div>
      </div>
    )
  }
}

export default GalleryItem;
