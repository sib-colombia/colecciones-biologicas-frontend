import React, { Component } from 'react';
import { compact, map } from 'lodash';

class Contact extends Component {

  associatedParty(data) {
    let lis = [];
    for (const key in data) {
      if (data.hasOwnProperty(key)) {
        let element = data[key];
        if (typeof element !== 'object') {
          element = ""+element;
          if (element.match(/^.*@.*$/))
            element = <a className="uk-text-tertiary" href={`mailto:${element}`}>{element}</a>

          lis.push(<li key={key} className="uk-text-break">{element}</li>)
        } else {
          if (key !== 'individualName') {
            map(element, (v, k) => {
              lis.push(<li key={k} className="uk-text-break">{v}</li>)
            })
          }
        }
      }
    }

    return map(compact(lis), (v) => { return v })
  }

  render() {
    const { data } = this.props;
    return (
      <div>
        <div className="uk-padding-small uk-margin-small-top">
          <h5 className="uk-text-bold uk-margin-small">
            {data.individualName && 
              <span>
              {data.individualName.givenName && data.individualName.givenName} {data.individualName.surName  && data.individualName.surName}
              </span>
            }
          </h5>
          <ul className="uk-list uk-padding-small" style={{ borderLeft: 'solid 2px #eee' }}>
            {this.associatedParty(data)}
            {/*map(rows, (value, key) => (<li key={key} className="uk-text-break">{value}</li>))*/}
          </ul>
        </div>
      </div>
    );
  }
}

export default Contact;
