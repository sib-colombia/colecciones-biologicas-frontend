import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import * as OccurrenceService from '../../services/OccurrenceService';

class GalleryCard extends Component {

  constructor(props) {
    super(props);
    this.state = {
      url: false
    }
  }
  capitalize(d) {
    const string = d.charAt(0).toUpperCase() + (d.slice(1)).toLowerCase();
    return string;
  }
  
  componentWillMount() {
    this.imagen(this.props.occurrence._source)
  }
  imagen(d){
    if(d.mediaType && d.mediaType==="StillImage"){
      OccurrenceService.getThumbID(d).then(data => {
        console.log("Consultando imágen: ", data, OccurrenceService.getThumb(data.url))
        this.setState({"url": OccurrenceService.getThumb(data.url)})
      });
    }
  }

  render() {

    const occurrence = this.props.occurrence._source;


    return <div id={this.props.scroll} className='uk-card uk-card-default uk-padding'>
              <div className="uk-flex uk-flex-column">
                <Link className="uk-link-reset" to={`/occurrence/${occurrence.occurrenceID}`}>
                  { this.state.url && <img src={this.state.url} alt="." /> }
                  { !this.state.url && <img src='/static/css/images/foto_especimen.png' alt="." /> }
                  <span className="scientificName">{occurrence.scientificName}</span>
                  <span>{occurrence.catalogNumber}</span>
                  </Link>
              </div>
            </div>
  }
}

export default GalleryCard;
