import React, { Component } from 'react';
import { Map, TileLayer, WMSTileLayer, LayersControl } from 'react-leaflet';

class HumboldtMap extends Component {

  constructor(props) {
    super(props);

    this.state = {
      z: this.props.zoom ? this.props.zoom : 6,
      c: this.props.center ? this.props.center : [4.36, -74.04],
      anteriorH: -1,
      anteriorW: -1,
      minHeigth: this.props.minHeigth ? this.props.minHeigth : 450
    }
  }

  actualizar() {
    // console.log("Funcion Actualizando")
    setTimeout(() => {
      let ref = this.map
      if (ref != null) {
        // console.log(ref)
        // console.log("Actualizando ", this.state.anteriorH, ref.clientHeight, ", ", ref.clientWidth)
        let h = ref.clientHeight
        if (h < this.state.minHeigth)
          h = this.state.minHeigth
        // console.log("Llamando el setState ", this.state.anteriorH, h)
        if (this.state.anteriorH === -1 || this.state.anteriorH === 0 || this.state.anteriorW === -1 || this.state.anteriorW === 0) {
          this.setState({ height: h, width: ref.clientWidth, anteriorH: ref.clientHeight, anteriorW: ref.clientWidth })
        } else {
          //this.setState({anteriorH: ref.clientHeight, anteriorW: ref.clientWidth})
        }
      }
    }, 1)
  }

  componentWillUpdate(nextProps, nextState) {
    let ref = this.map
    // console.log("Se quiere actualizar ", ref.clientHeight, ", ", ref.clientWidth)
    // console.log("El actual es ", this.state.height, ", ", this.state.width)
    // console.log("nextProps", nextProps)
    // console.log("nextState", nextState)
    if (
      ref.clientHeight > 0 &&
      ref.clientWidth > 0 &&
      (this.state.height === 0 || this.state.width === 0)
    ) {
      setTimeout(() => {
        this.setState({ height: ref.clientHeight, width: ref.clientWidth, anteriorH: ref.clientHeight, anteriorW: ref.clientWidth })
      }, 500)
    }
  }

  componentDidUpdate() {
    //let ref = this.map
    // console.log("Se actualizó ", ref.clientHeight, ", ", ref.clientWidth)
    //this.actualizar()
  }
  componentDidMount() {
    this.actualizar()
    //window.addEventListener('resize', this.updateDimensions)
  }

  // componentWillUnmount() { window.removeEventListener('resize', this.updateDimensions) }

  //updateDimensions() { this.setState({ width: window.innerWidth, height: window.innerHeight }) }


  render() {
    // console.log("Render = ", this.state.height, this.state.width)
    return (
      <div ref={el => this.map = el} className="uk-heigth-1-1">
        {
          this.state.height &&
          <Map style={{ width: this.state.width, height: this.state.height }} center={this.state.c} zoom={this.state.z} scrollWheelZoom={false}>
            <LayersControl position='topright'>
              <LayersControl.BaseLayer name='OpenStreetMap.BlackAndWhite' checked>
                <TileLayer
                  attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                  url='https://tiles.wmflabs.org/bw-mapnik/{z}/{x}/{y}.png'
                />
              </LayersControl.BaseLayer>

              <LayersControl.BaseLayer name='OpenStreetMap.Mapnik'>
                <TileLayer
                  attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                  url='https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png'
                />
              </LayersControl.BaseLayer>

              <LayersControl.BaseLayer name='Cartodb Basemaps'>
                <TileLayer url='https://cartodb-basemaps-{s}.global.ssl.fastly.net/dark_all/{z}/{x}/{y}.png' />
              </LayersControl.BaseLayer>

              {this.props.children}

              <LayersControl.Overlay name='Límite departamental de Colombia'>
                <WMSTileLayer
                  url='http://mapas.parquesnacionales.gov.co/services/pnn/wms'
                  layers='pnn:departamentos'
                  format='image/png'
                  transparent='true'
                  attribution='&copy; Parques Nacionales'
                />
              </LayersControl.Overlay>

              <LayersControl.Overlay name='Límite municipal de Colombia'>
                <WMSTileLayer
                  url='http://mapas.parquesnacionales.gov.co/services/pnn/wms'
                  layers='pnn:municipios'
                  format='image/png'
                  transparent='true'
                  attribution='&copy; Parques Nacionales'
                />
              </LayersControl.Overlay>

              <LayersControl.Overlay name='Parques Nacionales Naturales de Colombia'>
                <WMSTileLayer
                  url='http://mapas.parquesnacionales.gov.co/services/pnn/wms'
                  layers='pnn:area_protegida'
                  format='image/png'
                  transparent='true'
                  attribution='&copy; Parques Nacionales'
                />
              </LayersControl.Overlay>

              <LayersControl.Overlay name='Otras Áreas protegidas'>
                <WMSTileLayer
                  url='http://mapas.parquesnacionales.gov.co/services/pnn/wms'
                  layers='pnn:otras_areas_sinap'
                  format='image/png'
                  transparent='true'
                  attribution='&copy; Parques Nacionales'
                />
              </LayersControl.Overlay>

              <LayersControl.Overlay name='Reservas Naturales de la Sociedad Civil'>
                <WMSTileLayer
                  url='http://mapas.parquesnacionales.gov.co/services/pnn/wms'
                  layers='pnn:rnsc'
                  format='image/png'
                  transparent='true'
                  attribution='&copy; Parques Nacionales'
                />
              </LayersControl.Overlay>

              <LayersControl.Overlay name='Corporaciones Autónomas regionales'>
                <WMSTileLayer
                  url='http://mapas.parquesnacionales.gov.co/services/pnn/wms'
                  layers='pnn:car'
                  format='image/png'
                  transparent='true'
                  attribution='&copy; Parques Nacionales'
                />
              </LayersControl.Overlay>
            </LayersControl>
          </Map>
        }
      </div>
    )
  }
}

export default HumboldtMap;
