import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Marker } from 'react-leaflet';
import { filter, map, union, indexOf, fill } from 'lodash';
import StackGrid from "react-stack-grid";

import GenericPage from '../../templates/GenericPage';
import Header from '../../organisms/Header';
import Footer from '../../organisms/Footer';
import Details from '../../molecules/Details';
import HumboldtMap from '../../molecules/HumboldtMap';
import ExtensionTable from '../../molecules/ExtensionTable';
import * as OccurrenceService from '../../../services/OccurrenceService';
import Loading from '../../atoms/Loading';
import Gallery from '../../organisms/Gallery';

class Occurrence extends Component {

  constructor(props) {
    super(props);
    this.state = {
      data: null,
      imgs: null,
      imgf: null,
      isOpen: false,
      photoIndex: 0,
    }
  }

  organismoVacio(data) {
    if ((!data.organismQuantity || data.organismQuantity === '') &&
      (!data.organismQuantityType || data.organismQuantityType === '') &&
      (!data.organismName || data.organismName === '') &&
      (!data.organismScope || data.organismScope === '') &&
      (!data.associatedOrganisms || data.associatedOrganisms === '') &&
      (!data.organismRemarks || data.organismRemarks === '') &&
      (!data.previousIdentifications || data.previousIdentifications === '') &&
      (!data.associatedOccurrences || data.associatedOccurrences === ''))
      return true
    return false
  }

  registroVacio(data) {
    if ((!data.occurrenceID || data.occurrenceID === '') &&
      (!data.catalogNumber || data.catalogNumber === '') &&
      (!data.occurrenceRemarks || data.occurrenceRemarks === '') &&
      (!data.recordNumber || data.recordNumber === '') &&
      (!data.recordedBy || data.recordedBy === '') &&
      (!data.organismID || data.organismID === '') &&
      (!data.individualCount || data.individualCount === undefined) &&
      (!data.sex || data.sex === '') &&
      (!data.lifeStage || data.lifeStage === '') &&
      (!data.reproductiveCondition || data.reproductiveCondition === '') &&
      (!data.behavior || data.behavior === '') &&
      (!data.establishmentMeans || data.establishmentMeans === '') &&
      (!data.occurrenceStatus || data.occurrenceStatus === '') &&
      (!data.preparations || data.preparations === '') &&
      (!data.disposition || data.disposition === '') &&
      (!data.otherCatalogNumbers || data.otherCatalogNumbers === '') &&
      (!data.associatedMedia || data.associatedMedia === '') &&
      (!data.associatedReferences || data.associatedReferences === '') &&
      (!data.associatedSequences || data.associatedSequences === '') &&
      (!data.associatedTaxa || data.associatedTaxa === ''))
      return true
    return false
  }
  elementoVacio(data) {
    if ((!data.basisOfRecord || data.basisOfRecord === '') &&
      (!data.institutionCode || data.institutionCode === '') &&
      (!data.collectionCode || data.collectionCode === '') &&
      (!data.type || data.type === '') &&
      (!data.modified || data.modified === '') &&
      (!data.language || data.language === '') &&
      (!data.license || data.license === '') &&
      (!data.rightsHolder || data.rightsHolder === '') &&
      (!data.accessRights || data.accessRights === '') &&
      (!data.bibliographicCitation || data.bibliographicCitation === '') &&
      (!data.references || data.references === '') &&
      (!data.institutionID || data.institutionID === '') &&
      (!data.collectionID || data.collectionID === '') &&
      (!data.datasetID || data.datasetID === '') &&
      (!data.datasetName || data.datasetName === '') &&
      (!data.ownerInstitutionCode || data.ownerInstitutionCode === '') &&
      (!data.informationWithheld || data.informationWithheld === '') &&
      (!data.dataGeneralizations || data.dataGeneralizations === '') &&
      (!data.dynamicProperties || data.dynamicProperties === ''))
      return true
    return false
  }
  eventoVacio(data) {
    if ((!data.eventID || data.eventID === '') &&
      (!data.parentEventID || data.parentEventID === '') &&
      (!data.samplingProtocol || data.samplingProtocol === '') &&
      (!data.sampleSizeValue || data.sampleSizeValue === '') &&
      (!data.sampleSizeUnit || data.sampleSizeUnit === '') &&
      (!data.samplingEffort || data.samplingEffort === '') &&
      (!data.eventDate || data.eventDate === '') &&
      (!data.eventTime || data.eventTime === '') &&
      (!data.startDayOfYear || data.startDayOfYear === '') &&
      (!data.endDayOfYear || data.endDayOfYear === '') &&
      (!data.year || data.year === '') &&
      (!data.month || data.month === '') &&
      (!data.day || data.day === '') &&
      (!data.verbatimEventDate || data.verbatimEventDate === '') &&
      (!data.habitat || data.habitat === '') &&
      (!data.fieldNumber || data.fieldNumber === '') &&
      (!data.fieldNotes || data.fieldNotes === '') &&
      (!data.eventRemarks || data.eventRemarks === ''))
      return true
    return false
  }
  ubicacionVacio(data) {
    if ((!data.locationID || data.locationID === '') &&
      (!data.higherGeographyID || data.higherGeographyID === '') &&
      (!data.higherGeography || data.higherGeography === '') &&
      (!data.continent || data.continent === '') &&
      (!data.waterBody || data.waterBody === '') &&
      (!data.islandGroup || data.islandGroup === '') &&
      (!data.island || data.island === '') &&
      (!data.country || data.country === '') &&
      (!data.countryCode || data.countryCode === '') &&
      (!data.stateProvince || data.stateProvince === '') &&
      (!data.county || data.county === '') &&
      (!data.municipality || data.municipality === '') &&
      (!data.locality || data.locality === '') &&
      (!data.verbatimLocality || data.verbatimLocality === '') &&
      (!data.verbatimElevation || data.verbatimElevation === '') &&
      (!data.minimumElevationInMeters || data.minimumElevationInMeters === '') &&
      (!data.maximumElevationInMeters || data.maximumElevationInMeters === '') &&
      (!data.verbatimDepth || data.verbatimDepth === '') &&
      (!data.minimumDepthInMeters || data.minimumDepthInMeters === '') &&
      (!data.maximumDepthInMeters || data.maximumDepthInMeters === '') &&
      (!data.minimumDistanceAboveSurfaceInMeters || data.minimumDistanceAboveSurfaceInMeters === '') &&
      (!data.maximumDistanceAboveSurfaceInMeters || data.maximumDistanceAboveSurfaceInMeters === '') &&
      (!data.locationAccordingTo || data.locationAccordingTo === '') &&
      (!data.locationRemarks || data.locationRemarks === '') &&
      (!data.verbatimCoordinates || data.verbatimCoordinates === '') &&
      (!data.verbatimLatitude || data.verbatimLatitude === '') &&
      (!data.verbatimLongitude || data.verbatimLongitude === '') &&
      (!data.verbatimCoordinateSystem || data.verbatimCoordinateSystem === '') &&
      (!data.verbatimSRS || data.verbatimSRS === '') &&
      (!data.decimalLatitude || data.decimalLatitude === '') &&
      (!data.decimalLongitude || data.decimalLongitude === '') &&
      (!data.geodeticDatum || data.geodeticDatum === '') &&
      (!data.coordinateUncertaintyInMeters || data.coordinateUncertaintyInMeters === '') &&
      (!data.coordinatePrecision || data.coordinatePrecision === '') &&
      (!data.pointRadiusSpatialFit || data.pointRadiusSpatialFit === '') &&
      (!data.footprintWKT || data.footprintWKT === '') &&
      (!data.footprintSRS || data.footprintSRS === '') &&
      (!data.footprintSpatialFit || data.footprintSpatialFit === '') &&
      (!data.georeferencedBy || data.georeferencedBy === '') &&
      (!data.georeferencedDate || data.georeferencedDate === '') &&
      (!data.georeferenceProtocol || data.georeferenceProtocol === '') &&
      (!data.georeferenceSources || data.georeferenceSources === '') &&
      (!data.georeferenceVerificationStatus || data.georeferenceVerificationStatus === '') &&
      (!data.georeferenceRemarks || data.georeferenceRemarks === ''))
      return true
    return false
  }
  taxonVacio(data) {
    if ((!data.taxonID || data.taxonID === '') &&
      (!data.scientificNameID || data.scientificNameID === '') &&
      (!data.acceptedNameUsageID || data.acceptedNameUsageID === '') &&
      (!data.parentNameUsageID || data.parentNameUsageID === '') &&
      (!data.originalNameUsageID || data.originalNameUsageID === '') &&
      (!data.nameAccordingToID || data.nameAccordingToID === '') &&
      (!data.namePublishedInID || data.namePublishedInID === '') &&
      (!data.taxonConceptID || data.taxonConceptID === '') &&
      (!data.scientificName || data.scientificName === '') &&
      (!data.acceptedNameUsage || data.acceptedNameUsage === '') &&
      (!data.parentNameUsage || data.parentNameUsage === '') &&
      (!data.originalNameUsage || data.originalNameUsage === '') &&
      (!data.nameAccordingTo || data.nameAccordingTo === '') &&
      (!data.namePublishedIn || data.namePublishedIn === '') &&
      (!data.namePublishedInYear || data.namePublishedInYear === '') &&
      (!data.higherClassification || data.higherClassification === '') &&
      (!data.kingdom || data.kingdom === '') &&
      (!data.phylum || data.phylum === '') &&
      (!data.class || data.class === '') &&
      (!data.order || data.order === '') &&
      (!data.family || data.family === '') &&
      (!data.genus || data.genus === '') &&
      (!data.subgenus || data.subgenus === '') &&
      (!data.specificEpithet || data.specificEpithet === '') &&
      (!data.infraspecificEpithet || data.infraspecificEpithet === '') &&
      (!data.taxonRank || data.taxonRank === '') &&
      (!data.verbatimTaxonRank || data.verbatimTaxonRank === '') &&
      (!data.scientificNameAuthorship || data.scientificNameAuthorship === '') &&
      (!data.vernacularName || data.vernacularName === '') &&
      (!data.nomenclaturalCode || data.nomenclaturalCode === '') &&
      (!data.taxonomicStatus || data.taxonomicStatus === '') &&
      (!data.nomenclaturalStatus || data.nomenclaturalStatus === '') &&
      (!data.taxonRemarks || data.taxonRemarks === ''))
      return true
    return false
  }
  identificacionVacio(data) {
    if ((!data.identificationID || data.identificationID === '') &&
      (!data.identifiedBy || data.identifiedBy === '') &&
      (!data.dateIdentified || data.dateIdentified === '') &&
      (!data.identificationReferences || data.identificationReferences === '') &&
      (!data.identificationVerificationStatus || data.identificationVerificationStatus === '') &&
      (!data.identificationRemarks || data.identificationRemarks === '') &&
      (!data.identificationQualifier || data.identificationQualifier === '') &&
      (!data.typeStatus || data.typeStatus === ''))
      return true
    return false
  }
  muestraVacio(data) {
    if ((!data.materialSampleID || data.materialSampleID === ''))
      return true
    return false
  }
  contextoGeologicoVacio(data) {
    if ((!data.geologicalContextID || data.geologicalContextID === '') &&
      (!data.earliestEonOrLowestEonothem || data.earliestEonOrLowestEonothem === '') &&
      (!data.latestEonOrHighestEonothem || data.latestEonOrHighestEonothem === '') &&
      (!data.earliestEraOrLowestErathem || data.earliestEraOrLowestErathem === '') &&
      (!data.latestEraOrHighestErathem || data.latestEraOrHighestErathem === '') &&
      (!data.earliestPeriodOrLowestSystem || data.earliestPeriodOrLowestSystem === '') &&
      (!data.latestPeriodOrHighestSystem || data.latestPeriodOrHighestSystem === '') &&
      (!data.earliestEpochOrLowestSeries || data.earliestEpochOrLowestSeries === '') &&
      (!data.latestEpochOrHighestSeries || data.latestEpochOrHighestSeries === '') &&
      (!data.earliestAgeOrLowestStage || data.earliestAgeOrLowestStage === '') &&
      (!data.latestAgeOrHighestStage || data.latestAgeOrHighestStage === '') &&
      (!data.lowestBiostratigraphicZone || data.lowestBiostratigraphicZone === '') &&
      (!data.highestBiostratigraphicZone || data.highestBiostratigraphicZone === '') &&
      (!data.lithostratigraphicTerms || data.lithostratigraphicTerms === '') &&
      (!data.group || data.group === '') &&
      (!data.formation || data.formation === '') &&
      (!data.member || data.member === '') &&
      (!data.bed || data.bed === ''))
      return true
    return false
  }

  componentWillMount() {
    if (this.props.match.params.id !== undefined) {
      OccurrenceService.getOccurrence(this.props.match.params.id).then(data => {
        this.setState({ respuesta: data})
      }).catch(err => {
        console.log(err)
      })
      /*
      OccurrenceService.getImagesList().then(data => {
        this.setState({ imgs: data.imagesBase, imgf: data.images })
      }).catch(err => {
        console.log(err)
      })
      */
    }
  }


  render() {

    let sonidos = []
    let videos = []
    let imagenes = []
    
    const { respuesta } = this.state;
    
    
    if (respuesta===undefined){
      return (
          <GenericPage titlep="Búsqueda - Colecciones en Linea" header={<Header />} footer={<Footer />}>
            <Loading />
          </GenericPage>
        )
    }
    
    
    let occurrenceBasic = respuesta.occurrence
    let d = respuesta.dataset
    let p = respuesta.organization
    

    
    console.log(occurrenceBasic)
    
    let o = occurrenceBasic;
    let basicInformation = occurrenceBasic && {
      scientificName: occurrenceBasic.scientificName,
      breadcrumb: {
        kingdom: occurrenceBasic.kingdom,
        phylum: occurrenceBasic.phylum,
        order: occurrenceBasic.order,
        family: occurrenceBasic.family,
        genus: occurrenceBasic.genus,
        specificEpithet: occurrenceBasic.specificEpithet,
      },
      breadcrumb2: [
        occurrenceBasic.kingdom,
        occurrenceBasic.phylum,
        occurrenceBasic.class,
        occurrenceBasic.order,
        occurrenceBasic.family,
        occurrenceBasic.genus,
        occurrenceBasic.specificEpithet,
      ],
      country: occurrenceBasic.country,
      stateProvince: occurrenceBasic.stateProvince,
      basisOfRecord: occurrenceBasic.basisOfRecord,
      habitat: occurrenceBasic.habitat,
      resourceName: occurrenceBasic.datasetName,
      providerName: occurrenceBasic.provider_name,
      decimalLongitude: occurrenceBasic.decimalLongitude,
      decimalLatitude: occurrenceBasic.decimalLatitude,
      description: occurrenceBasic.abstract

    }

    if (o && o.child.multimedia && o.child.multimedia.length>0){
      imagenes = filter(o.child.multimedia[0], function (x) { return x.type === "StillImage" || x.type === "Imagen"; })
      sonidos = filter(o.child.multimedia[0], function (x) { return x.type === "Sound" || x.type === "Sonido"; })
      videos = filter(o.child.multimedia[0], function (x) { return x.type === "MovingImage" || x.type === "Vídeo"; })
      
    }

    if (basicInformation){
      while (basicInformation.breadcrumb2.slice(-1)[0]===""){
        basicInformation.breadcrumb2.pop()
      }
      basicInformation.breadcrumb2.pop()
    }



    return (<GenericPage titlep="Búsqueda - Colecciones en Linea" header={<Header />} footer={<Footer />}>
      {
        (occurrenceBasic && <div className="uk-section uk-padding-remove-bottom">
          <div className="uk-flex uk-flex-center uk-grid-collapse" data-uk-grid="data-uk-grid">
            <div>
              <h4 className="uk-margin-left uk-heading-divider uk-margin-remove" style={{
                borderBottomColor: '#ff7847'
              }}>ESPÉCIMEN</h4>
            </div>
          </div>
          <div className="uk-flex uk-flex-center uk-grid-collapse uk-heading-divider uk-child-width-1-1" data-uk-grid="data-uk-grid">
            <div className="uk-container uk-width-5-6@m uk-width-2-3@l uk-text-center uk-margin-top uk-padding-remove">
              <h2 className="scientificName">{basicInformation.scientificName}</h2>
            </div>
            <div className="uk-flex uk-flex-center uk-margin-small-top">
              {
                map(basicInformation.breadcrumb2, (v, k) => {
                  const last = basicInformation.breadcrumb2.length-1;
                  return v && (
                    <div key={k}>
                      <h5 className={k===5?"scientificName":""}>{v}
                        {last !== k && <span data-uk-icon="icon: chevron-right; ratio: 0.85"></span>}
                      </h5>
                    </div>
                  )
                })
              }

            </div>

          </div>
          <div className="uk-flex uk-flex-center">
            <div className="uk-card uk-card-default uk-text-bold uk-text-lead uk-text-center" style={{ paddingTop: 3, paddingBottom: 3, marginTop: -20 }}>
              <span className="uk-margin-left uk-margin-right">{o.catalogNumber}</span>
            </div>
          </div>
          <div className="uk-section uk-section-small">
            <div className="uk-container uk-width-5-6@m uk-width-2-3@l">
              <div className="uk-child-width-1-2 uk-grid-small" data-uk-grid="data-uk-grid">
                <div>
                  <span className="uk-text-bold uk-margin-small-right">Especie:</span>
                  <a className="uk-text-tertiary" href="">{basicInformation.scientificName}</a>
                </div>
                <div>
                  <span className="uk-text-bold uk-margin-small-right">Colección:</span>
                  <Link className="uk-text-tertiary" to={`/collection/`+o.GBIFID}>{o.title}</Link>
                </div>
                <div>
                  <span className="uk-text-bold uk-margin-small-right">Verbatim Name:</span>NO ENCONTRADO*</div>
                <div>
                  <span className="uk-text-bold uk-margin-small-right">Publicador:</span>
                        {o.provider_registration &&
                          <Link
                            className="uk-text-tertiary"
                            to={`/provider/` + o.provider_registration.registration.registry.hostingOrganisation.key}
                          >{o.organizationName}
                          </Link>
                        }
                </div>
                <div>
                  <span className="uk-text-bold uk-margin-small-right">Ubicación:</span>
                  {o.stateProvince}, {o.country}</div>
                <div>
                  <span className="uk-text-bold uk-margin-small-right">Colector:</span> NO ENCONTRADO*
                </div>
                <div>
                  <span className="uk-text-bold uk-margin-small-right">Tipo de espécimen:</span> NO ENCONTRADO*
                </div>
                <div>
                  <span className="uk-text-bold uk-margin-small-right">Fecha colecta:</span>{o.eventDate!==null?o.eventDate:o.eventDateOrg}*
                </div>
              </div>
            </div>
          </div>
          {
            (o.hasLocation && o.decimalLatitude !== undefined && o.decimalLongitude !== undefined) &&
            <div className="uk-height-large">
              <HumboldtMap center={[o.decimalLatitude, o.decimalLongitude]} zoom={8}>
                <Marker position={{ lat: o.decimalLatitude, lon: o.decimalLongitude }} />
              </HumboldtMap>
            </div>
          }
          <div className="uk-section uk-section-small uk-section-default">
            <div className="uk-container uk-width-5-6@m uk-width-2-3@l">
              <div className="uk-flex uk-flex-column">
                <h3 className="uk-margin-left uk-text-bold">Sobre la colección</h3>
                <div className="uk-width-1-4 uk-width-small@s uk-background-primary" style={{
                  height: 2
                }}></div>
              </div>
              <div className="uk-column-1-2 uk-margin-top">
                <p>{d.eml.abstract}</p>
              </div>
            </div>
          </div>
          {o.child && o.child.multimedia && 
          <Gallery title="Multimedia">
            {imagenes.length > 0 &&
              <Gallery.Section
                snapshots={imagenes}
                photoIndex={(index) => this.setState({ photoIndex: index, isOpen: true })}
                columnWidth={280}
              />
            }
            {
              this.state.isOpen &&
              <Gallery.Box
                imgf={imagenes}
                photoIndex={this.state.photoIndex}
                close={(isOpen) => this.setState(isOpen)}
                index={(index) => this.setState(index)}
              />
            }
            {sonidos.length > 0 &&
              <Gallery.Sound
                sounds={sonidos}
                columnWidth={280}
              />
            }
            {videos.length > 0 &&
              <Gallery.Video
                videos={videos}
                columnWidth={280}
              />
            }
          </Gallery>}
          <div className="uk-section uk-section-small" style={{ backgroundColor: '#e5e5e5' }}>
            <div className="uk-container uk-width-5-6@m uk-width-2-3@l">
              {o.child.multimedia && o.child.multimedia.length>0 &&
                <ExtensionTable titulo="Multimedia" data={o.child.multimedia[0]} />
              }
            </div>
          </div>
          <div className="uk-section uk-section-default uk-section-small">
            <div className="uk-container uk-width-5-6@m uk-width-2-3@l">
              <StackGrid columnWidth={"50%"} monitorImagesLoaded gutterWidth={40} gutterHeight={20}>
                {!this.registroVacio(o) &&
                  <Details title="Espécimen">
                    <Details.Registro data={o} />
                  </Details>
                }
                {!this.elementoVacio(o) &&
                  <Details title="Elemento de registro">
                    <Details.Occurrence data={o} />
                  </Details>
                }
                {!this.eventoVacio(o) &&
                  <Details title="Evento">
                    <Details.Event data={o} />
                  </Details>
                }
                {!this.ubicacionVacio(o) &&
                  <Details title="Ubicación">
                    <Details.Location data={o} />
                  </Details>
                }
                {!this.taxonVacio(o) &&
                  <Details title="Taxón">
                    <Details.Taxon data={o} />
                  </Details>
                }
                {!this.identificacionVacio(o) &&
                  <Details title="Identificación">
                    <Details.Identification data={o} />
                  </Details>
                }
                {!this.organismoVacio(o) &&
                  <Details title="Organismo">
                    <Details.Organism data={o} />
                  </Details>
                }
                {!this.muestraVacio(o) &&
                  <Details title="Muestra">
                    <Details.Sample data={o} />
                  </Details>
                }
                { /*<Details title="Medidas o hechos">
                  <Details.MeasuresFacts data={e} />
                </Details>
                */ }
                {!this.contextoGeologicoVacio(o) &&
                  <Details title="Contexto geológico">
                    <Details.GeologicalContext data={o} />
                  </Details>
                }
                {/*
                <Details title="Recursos relacionados">
                  <Details.RelatedResources data={r} />
                </Details>
                { o.child && o.child.multimedia &&
                <Details title="Multimedia">
                  <Details.Multimedia data={o.child.multimedia} />
                </Details>
                }
                */}
              </StackGrid>
            </div>
          </div>
          <div className="uk-section uk-section-small" style={{ backgroundColor: '#e5e5e5' }}>
            <div className="uk-container uk-width-5-6@m uk-width-2-3@l">
              <ExtensionTable titulo="Evento de muestreo" data={o.child.event} />
              <ExtensionTable titulo="Medidas o hechos" data={o.child.measurementorfact} />
              <ExtensionTable titulo="Medidas o hechos extendido" data={o.child.extendedmeasurementorfact} />
              <ExtensionTable titulo="Recursos relacionados" data={o.child.resourcerelationship} />
              <ExtensionTable titulo="Referencias" data={o.child.reference} />
              <ExtensionTable titulo="Información de parcelas" data={o.child.releve} />
            </div>
          </div>
          
        </div>) || <div className="uk-flex uk-flex-center uk-flex-middle uk-padding" data-uk-height-viewport="offset-top: true"><Loading /></div>
      }
    </GenericPage>)
  }
}

export default Occurrence;
