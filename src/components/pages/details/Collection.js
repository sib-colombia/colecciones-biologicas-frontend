import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import HumboldtMap from '../../molecules/HumboldtMap';
import MarkerClusterGroup from 'react-leaflet-markercluster';
import { filter, map } from 'lodash';

import GeographicCoverages from '../../molecules/GeographicCoverages';
import capitalize from 'lodash/capitalize';
import { route } from '../../../util';

import GenericPage from '../../templates/GenericPage';
import Header from '../../organisms/Header';
import Footer from '../../organisms/Footer';
import Doi from '../../atoms/Doi';
import License from '../../atoms/License';
import GeneralStatistics from '../../molecules/GeneralStatistics';
import DataSheet from '../../molecules/DataSheet';
import * as CollectionsService from '../../../services/CollectionsService';
import Loading from '../../atoms/Loading';
import ContactsGrid from '../../organisms/ContactsGrid';
import OccurrencesTable from '../../organisms/OccurrencesTable';
import * as DatasetService from '../../../services/DatasetService';


class Collection extends Component {

  constructor(props) {
    super(props);
    this.state = {
      data: null
    }
  }

  componentWillMount() {
    if (this.props.match.params.id !== undefined) {
      CollectionsService.getDataset(this.props.match.params.id).then(data => {
        this.setState({ data: data })
      }).catch(err => {
        console.log(err)
      })
    }
  }

  taxonomyCoverage(data) {
    if (!data){
      return <div>.</div>
    }
    let keys = {};
    // eslint-disable-next-line
    data.map((value) => {
      // eslint-disable-next-line
      (value.coverages).map((coverage, key) => {
        // eslint-disable-next-line
        keys[coverage.rank.verbatim] = Array();
      });
      // eslint-disable-next-line
      (value.coverages).map((coverage, key) => {
        keys[coverage.rank.verbatim].push(coverage.scientificName)
      });
    });

    return (Object.keys(keys)).map((values, key) => (<div key={key} className="uk-padding-small">
      <h4 className="uk-text-bold uk-margin-small">{capitalize(values)}</h4>
      <div className="uk-column-1-3">
        {
          keys[values].map((value, key) => (<div key={key}>
            <a className="uk-text-tertiary" href={`/search?${values}Name=${value}`} target="_blank">{value}</a>
          </div>))
        }
      </div>
    </div>))
  }
  temporalCoverage(data) {
    data = data.rangeOfDates;
    const beginDate = data.beginDate.calendarDate;
    const endDate = data.endDate.calendarDate;

    return (
      <p>
        <span className="uk-text-bold uk-margin-small-left uk-margin-small-right">Inicio:</span>
        {beginDate}
        <span data-uk-icon="icon: arrow-right" />
        <span className="uk-text-bold uk-margin-small-right">Fin:</span>
        {endDate}
      </p>
    )
  }
  descargar(url) {
    window.open(url);
  }

  handleIdentifiers(data) {
    return data.map((value, key) => {
      return (value.type === 'DOI' && <Doi key={key} label={value.identifier} />) || ((value.type === 'UUID' && <p key={key}>
        <Link to={`/dataset/${value.identifier}`} className="uk-text-tertiary">{`${window.location.origin}/dataset/${value.identifier}`}</Link><br />
        <a href={`https://www.gbif.org/dataset/${value.identifier}`} className="uk-text-tertiary" target="_blank">https://www.gbif.org/dataset/{value.identifier}</a>
      </p>)) || (value.type && <p key={key}>
        <a href={value.identifier} target="_blank" className="uk-text-tertiary">{value.identifier}</a>
      </p>)
    })
  }

  render() {
    const { data } = this.state;
    
    let org = []
    if(data){
      org = data.eml.contact
      
      if (!Array.isArray(org))
        org = [org]
        

    }

    
    return (<GenericPage titlep="Colección - Colecciones en Linea" header={<Header />} footer={<Footer />}>
      {
        (data && <div className="uk-section uk-padding-remove-bottom">
          <div className="uk-flex uk-flex-center uk-grid-collapse" data-uk-grid="">
            <div>
              <h4 className="uk-margin-left uk-heading-divider uk-margin-remove" style={{
                borderBottomColor: '#ff7847'
              }}>COLECCIÓN</h4>
            </div>
          </div>
          <div className="uk-flex uk-flex-center uk-grid-collapse" data-uk-grid="data-uk-grid">
            <div className="uk-container uk-width-5-6@m uk-width-2-3@l uk-text-center uk-margin-top uk-padding-remove">
              <h2>{data.eml.titleResource}</h2>
            </div>
            <div className="uk-width-1-1 uk-text-center uk-margin-small-top">
              <h5 className="uk-text-tertiary">Publicado por
                <Link className="uk-text-tertiary uk-margin-small-left" to={`/provider/${data.organizationId}`} style={{
                  textDecoration: 'underline'
                }}>{route(data, ['organization', 'provider', 'title'])}</Link>
              </h5>
            </div>
            <div className="uk-grid-small uk-margin-small-top" data-uk-grid="data-uk-grid">
                { map(org, (c, k)=>{
                    <div key={k}>
                      <div>{c.individualName.givenName + ' ' + c.individualName.surName}</div>
                      <div>·</div>
                      <div><a className="uk-text-tertiary" href={'mailto:' + c.electronicMailAddress}>{c.electronicMailAddress}</a></div>
                    </div>
                  }
                )}
            </div>
          </div>
          <div className="uk-section uk-margin-large-top uk-heading-divider uk-padding-remove" style={{
            borderBottomWidth: 1
          }}>
            <div className="uk-container uk-container-small uk-padding-remove ">
              <div className="uk-grid-collapse uk-child-width-expand uk-margin uk-flex uk-flex-bottom" data-uk-grid="data-uk-grid">
                <div>
                  <ul className="uk-margin-medium-top" data-uk-tab="swiping: true;" data-uk-switcher="connect: #paginasT">
                    <li>
                      <a>COLECCIÓN</a>
                    </li>
                    <li>
                      <a>CONTACTO</a>
                    </li>
                    <li>
                      <a>VER EN TABLA</a>
                    </li>
                  </ul>
                </div>
                <div className="uk-width-auto">
                  <ul className="uk-margin-medium-top" data-uk-tab="swiping: false;">
                    <li>
                      <a className="uk-text-right">
                        <span className="uk-text-top" data-uk-icon="icon: download"></span>
                        Descarga</a>
                      <div data-uk-dropdown="mode: hover">
                          {
                            filter(data.eml_raw['eml:eml'].dataset.alternateIdentifier, (o) => { return o.match(/^.*resource.*$/) })
                              .map((value, key) => (
                                <ul className="uk-nav uk-dropdown-nav" key={key}>
                                  <li>
                                    <a className="uk-text-tertiary" href={value} target="_blank" onClick={() => { this.descargar(value.replace("resource", "archive")) }} >DwC-A</a>
                                  </li>
                                  <li>
                                    <a className="uk-text-tertiary" href={value.replace("resource", "rtf")} target="_blank" onClick={() => { this.descargar(value.replace("resource", "rtf")) }} >RTF</a>
                                  </li>
                                  <li>
                                    <a className="uk-text-tertiary" href={DatasetService.getUrlData(value.replace("resource", "archive"))} target="_blank" onClick={() => { this.descargar(DatasetService.getUrlData(value.replace("resource", "archive"))) }} >CSV</a>
                                  </li>
                                </ul>
                              ))
                          }
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
          <ul className="uk-switcher" id="paginasT"> 
            <div className="uk-section uk-section-xsmall">
              <div className="uk-flex uk-flex-center">
                <div className="uk-container uk-width-5-6@m uk-width-2-3@l">
                  <div className="uk-child-width-1-1 uk-child-width-expand@s" data-uk-grid="data-uk-grid">
                    <div className="uk-width-auto">
                      <div style={{
                        width: 235,
                        height: 235
                      }} className="uk-flex uk-flex-center uk-flex-middle">
                        <Link to=""><img className="uk-preserved-width" width="150" height="150" src={data.eml.resourceLogoUrl} alt="" /></Link>
                      </div>
                      <div className="uk-margin-small-top"><Doi label={data.eml.doi} /></div>
                    </div>
                    <div>
                      <p>{data.eml.abstract && data.eml.abstract.substring(0, 200)}...
                        <a href="#descripcion" data-uk-scroll="offset: 95" className="uk-text-tertiary" to="">Ver más</a>
                      </p>
                      <p className="uk-margin-small">
                        <span className="uk-text-bold uk-margin-small-right">Ultima modificacíon:</span>
                        {data.eml_raw["eml:eml"].dataset.pubDate}</p>
                      <div className="uk-margin-small">
                        <span className="uk-text-bold uk-margin-small-right">Licencia:</span>
                        <License id={data.eml.licence_url} />
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div className="uk-margin-top"><GeneralStatistics id="GBIFID" param={data.GBIFID} /></div>
              <div className="uk-height-large uk-margin-remove-top">
                <HumboldtMap zoom={5}>
                  {this.state.dataMapa && <MarkerClusterGroup markers={this.state.dataMapa} />}
                </HumboldtMap>
              </div>

              <div className="uk-section uk-section-xsmall">
                <div className="uk-flex uk-flex-center">
                  <div className="uk-container uk-width-5-6@m uk-width-2-3@l">
                    <div className="uk-grid-small" data-uk-grid="data-uk-grid">
                      <div className="uk-width-1-4">
                        <div className="uk-card uk-card-default uk-padding-small" data-uk-sticky="offset: 90">
                          <ul className="uk-list uk-list-large uk-list-bullet uk-margin-remove-bottom" uk-scrollspy-nav="closest: li; scroll: true; cls: uk-text-primary">
                            <li>
                              <a className="uk-link-reset" href="#descripcion" data-uk-scroll="offset: 90">Descripción</a>
                            </li>
                            <li>
                              <a className="uk-link-reset" href="#cobertura_temporal" data-uk-scroll="offset: 90">Temporal</a>
                            </li>
                            <li>
                              <a className="uk-link-reset" href="#cobertura_geografica" data-uk-scroll="offset: 90">Geográfica</a>
                            </li>
                            <li>
                              <a className="uk-link-reset" href="#cobertura_taxonomica" data-uk-scroll="offset: 90">Taxonómica</a>
                            </li>
                            <li>
                              <a className="uk-link-reset" href="#metodologia" data-uk-scroll="offset: 90">Método de muestreo</a>
                            </li>
                            {
                              data.eml_raw['eml:eml'].additionalMetadata.metadata.gbif.bibliography &&
                              data.eml_raw['eml:eml'].additionalMetadata.metadata.gbif.bibliography.citation.length > 0 &&
                              <li>
                                <a className="uk-link-reset" href="#bibliografia" data-uk-scroll="offset: 90">Bibliografía</a>
                              </li>
                            }
                            <li>
                              <a className="uk-link-reset" href="#partes_asociadas" data-uk-scroll="offset: 90">Partes asociadas</a>
                            </li>
                            <li>
                              <a className="uk-link-reset" href="#citacion" data-uk-scroll="offset: 90">Citación</a>
                            </li>
                          </ul>
                        </div>
                      </div>
                      <div className="uk-width-3-4">
                        <DataSheet scroll="descripcion" title="Descripción">
                          <p>{data.eml.abstract}</p>
                        </DataSheet>
                        <DataSheet scroll="cobertura_temporal" title="Cobertura temporal" className="uk-margin-top">
                          {
                            data.eml &&
                            this.temporalCoverage(data.eml_raw['eml:eml'].dataset.coverage.temporalCoverage)
                          }
                        </DataSheet>
                        <DataSheet scroll="cobertura_geografica" title="Cobertura geográfica" className="uk-margin-top">
                          <div className="uk-margin-small-top">
                            <div>{
                              data.geographicCoverages &&
                              data.geographicCoverages.map((value, key) => (<p key={key}>{value.description}</p>))}</div>
                          </div>
                        </DataSheet>
                        <div className="uk-height-large">
                          { data.geographicCoverages &&
                          <HumboldtMap zoom={5}><GeographicCoverages g={data.geographicCoverages} /></HumboldtMap>
                          }
                        </div>
                        <DataSheet scroll="cobertura_taxonomica" title="Cobertura taxonómica" className="uk-margin-top">
                          {this.taxonomyCoverage(data.taxonomicCoverages)}
                        </DataSheet>
                        <DataSheet scroll="metodologia" title="Metodología" className="uk-margin-top">
                          { data.eml &&
                            <div className="uk-padding-small">
                              {data.eml_raw['eml:eml'].dataset.methods.sampling.studyExtent.description.para && <h4 className="uk-text-bold uk-margin-small">Área de estudio</h4>}
                              {data.eml_raw['eml:eml'].dataset.methods.sampling.studyExtent.description.para && <p>{data.eml_raw['eml:eml'].dataset.methods.sampling.studyExtent.description.para}</p>}
                              {data.eml_raw['eml:eml'].dataset.methods.sampling.samplingDescription.para && <h4 className="uk-text-bold uk-margin-small">Muestreo</h4>}
                              {data.eml_raw['eml:eml'].dataset.methods.sampling.samplingDescription.para && <p>{data.eml_raw['eml:eml'].dataset.methods.sampling.samplingDescription.para}</p>}
                              {data.eml_raw['eml:eml'].dataset.methods.qualityControl.description.para && <h4 className="uk-text-bold uk-margin-small">Control de calidad</h4>}
                              {data.eml_raw['eml:eml'].dataset.methods.qualityControl.description.para && <p>{data.eml_raw['eml:eml'].dataset.methods.qualityControl.description.para}</p>}
                              <h4 className="uk-text-bold uk-margin-small">Descripción de la metodología paso a paso</h4>
                              {data.eml_raw['eml:eml'].dataset.methods.methodStep &&

                                  <div className="uk-child-width-expand uk-grid-small" data-uk-grid="data-uk-grid">
                                    <div className="uk-width-auto">
                                      <span className="uk-badge uk-text-bold">*</span>
                                    </div>
                                    <div>
                                      {data.eml_raw['eml:eml'].dataset.methods.methodStep.description.para}
                                    </div>
                                  </div>
                              }
                            </div>
                          }
                        </DataSheet>
                          {
                            data.eml_raw['eml:eml'].additionalMetadata.metadata.gbif.bibliography &&
                            data.eml_raw['eml:eml'].additionalMetadata.metadata.gbif.bibliography.citation.length > 0 && <DataSheet scroll="bibliografia" title="Bibliografía" className="uk-margin-top">
                              <ol>
                                {data.eml_raw['eml:eml'].additionalMetadata.metadata.gbif.bibliography.citation.map((value, key) => {
                                
                                  if (typeof value === 'string'){
                                    return <li key={key}>{value}</li>
                                  }else{
                                    return <li key={key}>{value["#text"]}</li>
                                  }
                                })}
                              </ol>
                            </DataSheet>
                          }
                        <DataSheet scroll="partes_asociadas" title="Partes asociadas" className="uk-margin-top">
                            <ContactsGrid data={data.eml_raw['eml:eml'].dataset.associatedParty} grid="1-2" />
                        </DataSheet>
                        {/*
                        <DataSheet scroll="registro_gbif" title="Registro en GBIF" className="uk-margin-top">
                          <table className="uk-table uk-table-small uk-margin-remove-bottom">
                            <tbody>
                              {
                                data.created && <tr>
                                  <td className="uk-text-bold">Fecha de registro</td>
                                  <td className="uk-table-expand">{new Date(data.created).toISOString().substring(0, 10)}</td>
                                </tr>
                              }
                              {
                                data.modified && <tr>
                                  <td className="uk-text-bold">Última edición</td>
                                  <td className="uk-table-expand">{new Date(data.modified).toISOString().substring(0, 10)}</td>
                                </tr>
                              }
                              {
                                data.pubDate && <tr>
                                  <td className="uk-text-bold">Fecha de publicación</td>
                                  <td className="uk-table-expand">{new Date(data.pubDate).toISOString().substring(0, 10)}</td>
                                </tr>
                              }
                              {
                                data.publishingOrganization && <tr>
                                  <td className="uk-text-bold">Host</td>
                                  <td className="uk-table-expand">{data.publishingOrganization.title}</td>
                                </tr>
                              }
                              <tr>
                                <td className="uk-text-bold">Instalación</td>
                                <td className="uk-table-expand">NO ENCONTRADO*</td>
                              </tr>
                              <tr>
                                <td className="uk-text-bold">Installation Contacts</td>
                                <td className="uk-table-expand">
                                  <ContactsGrid minified="minified" data={data.contacts} />
                                </td>
                              </tr>
                              <tr>
                                <td className="uk-text-bold">Endpoints</td>
                                <td className="uk-table-expand">
                                  {
                                    (data.endpoints).map((value, key) => (<p key={key}>
                                      <a className="uk-text-tertiary" href={value.url} target="_blank">{value.url}</a>
                                      ({value.type})</p>))
                                  }
                                </td>
                              </tr>
                              <tr>
                                <td className="uk-text-bold">Identifiers</td>
                                <td className="uk-table-expand">
                                  {this.handleIdentifiers(data.identifiers)}
                                </td>
                              </tr>
                              <tr>
                                <td className="uk-text-bold">Last succesful crawl</td>
                                <td className="uk-table-expand">NO ENCONTRADO*</td>
                              </tr>
                              <tr>
                                <td className="uk-text-bold">Crawling reasons</td>
                                <td className="uk-table-expand">NO ENCONTRADO*</td>
                              </tr>
                              <tr>
                                <td className="uk-text-bold">Crawls in total</td>
                                <td className="uk-table-expand">NO ENCONTRADO*</td>
                              </tr>
                            </tbody>
                          </table>
                        </DataSheet>
                        */}
                        <DataSheet scroll="citacion" title="Citación" className="uk-margin-top">
                          <p>{
                            data.eml && 
                            data.eml_raw['eml:eml'].additionalMetadata.metadata.gbif.citation["#text"]}</p>
                        </DataSheet>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div>
              <div className="uk-section uk-section-xsmall uk-section-default">
                <div className="uk-flex uk-flex-center">
                  <div className="uk-container uk-width-5-6@m uk-width-2-3@l">
                    {data.eml_raw['eml:eml'].dataset.project.personnel && <ContactsGrid data={[data.eml_raw['eml:eml'].dataset.project.personnel]} grid="1" />}
                  </div>
                </div>
              </div>
            </div>
            <div className="uk-section uk-section-small" >
              <div className="uk-container uk-width-5-6@m uk-width-2-3@l">
                <OccurrencesTable occurrences={(number) => console.log(number)}  search={"gbifId="+data.gbifId} />
              </div>
            </div>            
          </ul>
        </div>) || <div className="uk-flex uk-flex-center uk-flex-middle uk-padding" data-uk-height-viewport="offset-top: true"><Loading /></div>
      }
    </GenericPage>)
  }
}

export default Collection;
