import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import GenericPage from '../../templates/GenericPage';
import Header from '../../organisms/Header';
import Footer from '../../organisms/Footer';
import GeneralStatistics from '../../molecules/GeneralStatistics';
import * as ProviderService from '../../../services/ProviderService';
import Loading from '../../atoms/Loading';
import ContactsGrid from '../../organisms/ContactsGrid';
import * as CollectionsService from '../../../services/CollectionsService';
import CollectionsList from '../../organisms/CollectionsList';
import Pagination from '../../atoms/Pagination';


class Provider extends Component {

  constructor(props) {
    super(props);

    this.state = {
      data: null,
      dataC: null,
      count: '',
    }
  }

  componentWillMount() {
    if (this.props.match.params.id !== undefined) {
      ProviderService.getProvider(this.props.match.params.id).then(data => {
        this.setState({ data: data }, ()=>{
          this.offsetResults(0)
        })
      }).catch(err => {
        console.log(err)
      })
    }
  }

  componentDidMount() {
    this.offsetResults(0);
  }

  onChangePage(pageOfItems) {
    const page = pageOfItems * 20;
    this.offsetResults(page, pageOfItems);
  }

  offsetResults(offset, pageOfItems) {
    console.log("Cargando lista")
    CollectionsService.getDatasetList(offset, 'organizationId='+this.props.match.params.id).then(data => {
    
      console.log("lista cargada")
      console.log(data)

      this.setState({
        dataC: data.aggregations.gbifId.buckets,
        offset: data.offset,
        count: "",
        currentPage: pageOfItems
      })

    });
  }

  render() {
    let { data } = this.state;
    if (data)
      data = data.provider
    console.log(data)
    return (
      <GenericPage
        titlep="Publicador - Colecciones en Linea"
        header={<Header />}
        footer={<Footer />}
      >
        {(data && <div className="uk-section uk-padding-remove-bottom">
          <div className="uk-flex uk-flex-center uk-grid-collapse" data-uk-grid>
            <div>
              <h4 className="uk-margin-left uk-heading-divider uk-margin-remove" style={{ borderBottomColor: '#ff7847' }}>PUBLICADOR</h4>
            </div>
          </div>
          <div className="uk-flex uk-flex-center uk-grid-collapse" data-uk-grid>
            <div className="uk-container uk-width-5-6@m uk-width-2-3@l uk-text-center uk-padding-remove uk-margin-top">
              <h2>{data.title}</h2>
            </div>
          </div>
          <div className="uk-margin-top">
            <GeneralStatistics  id={"organizationId"} param={this.props.match.params.id} />
          </div>
          <div className="uk-flex uk-flex-center uk-margin-top uk-margin">
            <div className="uk-container uk-width-5-6@m uk-width-2-3@l">
              <div className="uk-child-width-1-1 uk-child-width-expand@s" data-uk-grid>
                <div className="uk-width-1-4">
                  <img className="uk-preserved-width" src={data.resourceLogoUrl} alt="" />
                  { data.homepage && data.homepage.length>0 &&
                    <p className="uk-text-bold"><span className="uk-text-top uk-margin-small-right" uk-icon="icon: link"></span><a className="uk-text-tertiary" href={data.homepage[0]} target="_blank" rel="">{data.homepage[0]}</a></p>
                  }
                </div>
                <div>
                  <p>{data.description}</p>
                  <p className="uk-text-bold"><span className="uk-margin-small-right">Herramienta de publicación</span> <Link className="uk-text-tertiary" to=" ">Enlance</Link></p>
                </div>
              </div>
            </div>
          </div>
          <div className="uk-section uk-section-small uk-section-default">
            <div className="uk-container uk-width-5-6@m uk-width-2-3@l">
              <div className="uk-flex uk-flex-column">
                <h3 className="uk-margin-left uk-text-bold">Contactos</h3>
                <div className="uk-width-1-4 uk-width-small@s uk-background-primary" style={{ height: 2 }}></div>
              </div>
              <ContactsGrid data={data.contacts} grid="1-3" />
            </div>
          </div>
          <div className="uk-section uk-section-small">
            <div className="uk-container uk-width-5-6@m uk-width-2-3@l">
              <div className="uk-flex uk-flex-column">
                <h3 className="uk-margin-left uk-text-bold">Colecciones</h3>
                <div className="uk-width-1-4 uk-width-small@s uk-background-primary" style={{ height: 2 }}></div>
              </div>
            </div>
            <div className="uk-container uk-width-5-6@m uk-width-2-3@l">
              {/*<ul className="uk-margin-medium-top uk-flex-right" data-uk-tab="swiping: false;" data-uk-switcher>
                <li>
                  <a className="uk-text-right" title="Ordenar" data-uk-tooltip>A/Z</a>
                  <div className="uk-padding-small" data-uk-dropdown="mode: click">
                    <ul className="uk-nav uk-dropdown-nav">
                      <li><a>Alfabético</a></li>
                      <li><a>Recientes</a></li>
                      <li><a>Número de registros</a></li>
                    </ul>
                  </div>
                </li>
              </ul>*/}
              <CollectionsList data={this.state.dataC} />
              {this.state.dataC && <Pagination items={this.state.count} onChangePage={(number) => {
                this.onChangePage(number - 1);
              }} />}
            </div>
          </div>
        </div>) || <div className="uk-flex uk-flex-center uk-flex-middle uk-padding" data-uk-height-viewport="offset-top: true"><Loading /></div>}
      </GenericPage>
    );
  }
}

export default Provider;
