import React, {Component} from 'react';
import {Link} from 'react-router-dom';

import GenericPage from '../templates/GenericPage';
import Header from '../organisms/Header';
import Footer from '../organisms/Footer';
import GeneralStatistics from '../molecules/GeneralStatistics';

class Home extends Component {

  constructor(props) {
    super(props);
    this.state = {
      height: NaN
    }

    this.updateDimensions = this.updateDimensions.bind(this);
  }

  componentWillMount() {
    this.updateDimensions()
  }

  componentDidMount() {
    window.addEventListener('resize', this.updateDimensions)
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.updateDimensions)
  }

  updateDimensions() {
    this.setState({
      height: (window.innerHeight / 2)
    })
  }

  render() {
    return (<GenericPage titlep="Colecciones en Linea" header={<Header />} footer={<Footer />}>
      <div className="uk-position-relative uk-light" data-uk-slideshow={`min-height: ${this.state.height}; max-height: ${this.state.height}`}>
        <ul className="uk-slideshow-items">
          <li>
            <img src="static/css/images/fondo.jpg" alt="" data-uk-cover="data-uk-cover"/>
            <div className="uk-overlay uk-overlay-primary uk-position-top-right uk-position-small uk-padding-small uk-text-center uk-light uk-visible@s">
              <p className="uk-text-small">Felipe Villegas / Instituto Humboldt</p>
            </div>
          </li>
        </ul>
        <button className="uk-position-center-left uk-position-small uk-visible@s" data-uk-slidenav-previous="ratio: 1.5" uk-slideshow-item="previous"/>
        <button className="uk-position-center-right uk-position-small uk-visible@s" data-uk-slidenav-next="ratio: 1.5" uk-slideshow-item="next"/>
        <div className="uk-position-center uk-position-medium">
          <div className="uk-flex uk-flex-column">
            <Link to="/search" style={{
                textDecoration: 'none'
              }}>
              <div className="uk-overlay uk-padding-small uk-text-center" style={{
                  border: 'solid 1.5px #fff',
                  cursor: 'pointer',
                  backgroundColor: (
                    !this.state.hover
                    ? 'rgba(0,0,0,0.2)'
                    : 'rgba(255,255,255,0.2)'),
                  transition: 'background-color 0.3s ease'
                }} onMouseEnter={() => this.setState({hover: true})} onMouseLeave={() => this.setState({hover: false})}>
                <h3 className="uk-text-bold">EXPLORA LOS ESPECÍMENES</h3>
              </div>
            </Link>
            <div className="uk-grid-small" data-uk-grid="data-uk-grid">
              <div>
                <Link to="/collections" className="uk-text-small uk-text-bold"><span className="uk-text-top" uk-icon="icon: triangle-right; ratio: 1.25"/>COLECCIONES</Link>
              </div>
              <div>
                <Link to="/providers" className="uk-text-small uk-text-bold"><span className="uk-text-top" uk-icon="icon: triangle-right; ratio: 1.25"/>PUBLICADORES</Link>
              </div>
            </div>
          </div>
        </div>
        <div className="uk-position-bottom-center uk-position-medium">
          <ul className="uk-dotnav uk-flex-nowrap uk-margin-bottom">
            <li uk-slideshow-item="0">
              <a>Item 1</a>
            </li>
          </ul>
        </div>
      </div>
      <div className="uk-flex uk-flex-center uk-margin-left uk-margin-right" style={{
          marginTop: -45
        }}>
        <div className="uk-container uk-width-5-6@m uk-width-2-3@l"><GeneralStatistics/></div>
      </div>
      <div className="uk-section">
        <div className="uk-container uk-width-5-6@m uk-width-2-3@l">
          <div className="uk-flex uk-flex-column">
            <h2 className="uk-margin-left">
              Explora a través de grupos biológicos
            </h2>
            <div className="uk-width-1-4 uk-width-small@s uk-background-primary" style={{
                height: 2
              }}></div>
          </div>
          <div className="uk-flex uk-flex-center uk-margin-top uk-child-width-1-1 uk-child-width-1-2@s uk-child-width-1-4@m uk-child-width-1-5@l uk-text-center" data-uk-grid="data-uk-grid">
            <Link className="uk-link-reset" to="/search?phylum=Acanthocephala&phylum=Annelida&phylum=Arthropoda&phylum=Brachiopoda&phylum=Bryozoa&phylum=Chaetognatha&phylum=Cnidaria&phylum=Echinodermata&phylum=Gastrotricha&phylum=Mollusca&phylum=Myxozoa&phylum=Nematoda&phylum=Onychophora&phylum=Platyhelminthes&phylum=Porifera&phylum=Rotifera&phylum=Sipuncula">
              <img className="uk-card uk-card-default uk-card-body uk-border-circle uk-padding-remove" src="https://statics.sibcolombia.net/sib-resources/images/portal-de-datos/png/h-portal-invertebrados.png" alt=""/>
              <p className="uk-text-center">INVERTEBRADOS</p>
            </Link>
            <Link className="uk-link-reset" to="/search?phylum=Chordata">
              <img className="uk-card uk-card-default uk-card-body uk-border-circle uk-padding-remove" src="https://statics.sibcolombia.net/sib-resources/images/portal-de-datos/png/h-portal-vertebrados.png" alt=""/>
              <p className="uk-text-center">VERTEBRADOS</p>
            </Link>
            <Link className="uk-link-reset" to="/search?kingdom=Plantae">
              <img className="uk-card uk-card-default uk-card-body uk-border-circle uk-padding-remove" src="https://statics.sibcolombia.net/sib-resources/images/portal-de-datos/png/h-portal-plantas.png" alt=""/>
              <p className="uk-text-center">PLANTAS</p>
            </Link>
            <Link className="uk-link-reset" to="/search?kingdom=Fungi">
              <img className="uk-card uk-card-default uk-card-body uk-border-circle uk-padding-remove" src="https://statics.sibcolombia.net/sib-resources/images/portal-de-datos/png/h-portal-hongos.png" alt=""/>
              <p className="uk-text-center">HONGOS</p>
            </Link>
            <Link className="uk-link-reset" to="/search?kingdom=Archaea&kingdom=Bacteria&kingdom=incertae%20sedis&kingdom=Protozoa&kingdom=Viruses">
              <img className="uk-card uk-card-default uk-card-body uk-border-circle uk-padding-remove" src="https://statics.sibcolombia.net/sib-resources/images/portal-de-datos/png/h-portal-otros.png" alt=""/>
              <p className="uk-text-center">MICROORGANISMOS</p>
            </Link>
          </div>
        </div>
      </div>
      <div className="uk-section uk-light" style={{
          backgroundColor: '#00a8b4'
        }}>
        <div className="uk-container uk-width-5-6@m uk-width-2-3@l">
          <div className="uk-flex uk-flex-center uk-flex-middle uk-child-width-1-1 uk-child-width-1-2@s" data-uk-grid="data-uk-grid">
            <div className="uk-text-center">
              <h1 className="uk-margin-remove">¿Quieres participar?</h1>
            </div>
            <div>
              <div className="uk-overlay uk-text-center uk-padding-small uk-link-reset" style={{
                  border: 'solid 1.5px #fff'
                }}>
                <Link to="/static/sobre_el_portal">
                  <h2 className="uk-margin-remove">ENTÉRATE CÓMO</h2>
                </Link>
              </div>
            </div>
          </div>
        </div>
      </div>
    </GenericPage>);
  }
}

export default Home;
