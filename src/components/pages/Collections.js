import React, { Component } from 'react';
import NumberFormat from 'react-number-format';
import cx from 'classnames';
import { map, split, indexOf, join, /* merge */ } from 'lodash';

import LogicPage from '../templates/LogicPage';
import Header from '../organisms/Header';
import Footer from '../organisms/Footer';
import Title from '../atoms/Title';
import CollectionsList from '../organisms/CollectionsList';
import CollectionsGrid from '../organisms/CollectionsGrid';
import Pagination from '../atoms/Pagination';
import * as DatasetService from '../../services/DatasetService';
import Filters from '../molecules/Filters';


class Collections extends Component {

  constructor(props) {
    super(props);
    this.state = {
      kind: 0,
      data: null,
      count: '',
      type: "ALL",
      amount: null,
      tab: 0,
      search: props.location.search      
    }

    this.count = [];
    this.search = [];
  }

  componentDidMount() {
    this.offsetResults(0, 0,this.state.search)
    this.setState({ modified: true })
  }

  onChangePage(pageOfItems) {
    const page = pageOfItems * 20;
    this.offsetResults(page, pageOfItems, this.state.search);
  }

  offsetResults(offset, pageOfItems, search) {
    DatasetService.getDatasetList(offset, search, this.state.orderBy)
      .then(data => {
        this.setState({
        data: data.aggregations.gbifId.buckets,
        count: data.aggregations.gbifId.doc_count_error_upper_bound,
        })
      })
      .catch(() => {
        this.setState({ 
          data: [],
          count: 0,
        })
      })
  }
  
  handleDisplay(kind) {this.setState({ kind })}

  cambiarTipo(tipo, num) {
    this.setState({ data: null, type: tipo, tab: num }, () => {
      this.offsetResults(0, 0, this.state.search)
    })
  }

  setCount(e, filter) {
    this.count[filter] = e.length;
    this.search[filter] = e;
    const stringQ = this.createQuery(this.search);
    window.history.replaceState('', 'Búsqueda - Colecciones en Linea', `${this.props.location.pathname}${stringQ && '?' + stringQ}`)
    this.offsetResults(0, 0, stringQ)

    this.setState({ amount: this.count, search: stringQ })
  }

  createQuery(data) {
    let search = this.props.location.search !== '' ? split((this.props.location.search).slice(1), '&') : [];
    search['query'] = []
    map(data, (value, key) => {
      map(value, (value1) => {
        const i = indexOf(search['query'], value1.id);
        if (i < 0) {
          search['query'].push(value1.id)
        }
      })
    })
    
    if (!this.state.modified && search['query'].length < 0)
      search['query'].unshift(join(search, '&'))

    return join(search['query'], '&');
  }

  activeFilters() {
    return this.props.location.search !== '' && split((this.props.location.search).slice(1), '&');
  }

  orderBy(orderBy) {
    this.setState({ orderBy }, () => this.offsetResults(0))
  }

  render() {
    return (
      <LogicPage
        titlep="Recursos - Colecciones en Linea"
        headercustom={<Header withSidebar={'sidebar-search'} search={this.props.location.search} />}
        sidebar={
          <Filters count={this.state.amount} search={this.state.search}>
            <Filters.ResourceName activeFilters={this.activeFilters()} count={(e) => this.setCount(e, 0)} />
            <Filters.ProviderName activeFilters={this.activeFilters()} count={(e) => this.setCount(e, 1)} />
            {/*
            <Filters.Project activeFilters={this.activeFilters()} count={(e) => this.setCount(e, 2)} />
            */}
            <Filters.EventDate activeFilters={this.activeFilters()} count={(e) => this.setCount(e, 3)} />
            {/*
            <Filters.License activeFilters={this.activeFilters()} count={(e) => this.setCount(e, 2)} />
            <Filters.Doi activeFilters={this.activeFilters()} count={(e) => this.setCount(e, 4)} />
            <Filters.ResourceName filters={this.state.filters} activeFilters={this.activeFilters()} count={(e) => this.setCount(e, 0)} />
            <Filters.ProviderName filters={this.state.filters} activeFilters={this.activeFilters()} count={(e) => this.setCount(e, 1)} />
            <Filters.CollectionType filters={this.state.filters} activeFilters={this.activeFilters()} count={(e) => this.setCount(e, 2)} />
            <Filters.License filters={this.state.filters} activeFilters={this.activeFilters()} count={(e) => this.setCount(e, 2)} />
            <Filters.EventDate filters={this.state.filters} activeFilters={this.activeFilters()} count={(e) => this.setCount(e, 3)} />
            <Filters.Doi filters={this.state.filters} activeFilters={this.activeFilters()} count={(e) => this.setCount(e, 4)} />
            */}
          </Filters>
        }
        footer={<Footer />}
      >
        <div className="uk-section uk-section-small">
          <div className="uk-container uk-container-expand">
            <Title
              label="BÚSQUEDA POR COLECCIÓN"
              icon={<span className="uk-text-primary" uk-icon="icon: triangle-right; ratio: 1.3" />}
              number={<NumberFormat value={this.state.count} displayType="text" thousandSeparator />}
              labelNumber="RESULTADOS"
            />
            <div className="uk-grid-collapse uk-child-width-expand uk-margin uk-flex uk-flex-bottom" data-uk-grid>
              <div className="uk-width-expand">
                <ul className="uk-margin-medium-top uk-flex-right" data-uk-tab="swiping: false;" data-uk-switcher>
                  <li className={cx({ 'uk-active': !this.state.displayDatasets })} onClick={() => this.handleDisplay(false)}><a title="Modo lista" data-uk-tooltip><span uk-icon="icon: list"></span></a></li>
                  <li className={cx({ 'uk-active': this.state.displayDatasets })} onClick={() => this.handleDisplay(true)}><a title="Moto grilla" data-uk-tooltip><span uk-icon="icon: grid"></span></a></li>
                  {/*<li>
                    <a className="uk-text-right" title="Ordenar" data-uk-tooltip>A/Z</a>
                    <div className="uk-padding-small" data-uk-dropdown="mode: click">
                      <ul className="uk-nav uk-dropdown-nav">
                        <li onClick={() => this.orderBy('alpha')}><a>Alfabético</a></li>
                        <li onClick={() => this.orderBy('recent')}><a>Recientes</a></li>
                        <li onClick={() => this.orderBy('NRecords')}><a>Número de registros</a></li>
                      </ul>
                    </div>
                  </li>*/}
                </ul>
              </div>
            </div>
            {!this.state.kind ? <CollectionsList data={this.state.data} /> : <CollectionsGrid data={this.state.data} />}
          </div>
        </div>
        {/*{this.state.data && <Pagination items={this.state.count} onChangePage={(number) => {
              this.onChangePage(number - 1);
            }}/>}*/}cd
      </LogicPage>
    )
  }
}


export default Collections;
