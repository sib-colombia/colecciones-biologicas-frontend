export const URL_LOGO = 'https://statics.sibcolombia.net/sib-resources/images/logos-canales/png/logo-colecciones-corte.png';
export const MAPBOX_TOKEN = process.env.REACT_APP_MAPBOX_TOKEN
export const URL_API = process.env.REACT_APP_URL_API
export const URL_GEOJSON = 'http://static.biodiversidad.co';
export const URL_API_IDE = 'http://beta.datos.biodiversidad.co';
export const URL_API_USERS = 'http://apiusers.biodiversidad.co';
//export const URL_API_USERS = 'http://localhost:8000';
export const PATH = window.location.origin.indexOf('localhost') > -1 ? 'http://localhost:3000' : window.location.origin;
export const TOKEN = 'token-colecciones'
export const USER = 'portal-user'
export const URL_GBIF = 'http://api.gbif.org';


export const URL_LISTAS = 'http://listas.biodiversidad.co/';
export const URL_COLECCIONES = 'http://colecciones.biodiversidad.co/';
export const URL_PORTAL = 'http://datos.biodiversidad.co/';
export const URL_CATALOGO = 'http://catalogo.biodiversidad.co/';


export const URL_ES = process.env.REACT_APP_URL_ES;
export const URL_MONGO = process.env.REACT_APP_URL_MONGO;

export const URL_STATICS = 'http://167.114.113.179:8000';

export const KEY_TYPE = 'specimen';
//export const KEY_TYPE = 'occurrence'

//export const THUMBS_API = window.location.origin.indexOf('localhost') > -1 ? 'http://localhost:8000/api/images/image' : 'http://beta.colecciones.biodiversidad.co/api/images/image';
export const THUMBS_API = 'http://beta.colecciones.biodiversidad.co/api/images/image';
