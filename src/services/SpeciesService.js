import fetch from 'isomorphic-fetch';

import { URL_ES, URL_API, KEY_TYPE } from '../config/const';
import { request } from '../util'

const xhr = new XMLHttpRequest()

export function getSpeciesList(limit, search) {
  if (search && search.indexOf('?') >= 0) {
    search = search.slice(1)
  }

  return request(xhr, 'GET', `${URL_ES}/api/agg2?agg=scientificName&keyType=${KEY_TYPE}&limit=${limit}${search && '&' + search}`)
}


export function getSpecies(id) {
  return fetch(`${URL_API}/api/species/basic/` + id, { method: 'GET' }).then((response) => {
    return response.json()
  }).then((data) => {
    return data
  })
}


