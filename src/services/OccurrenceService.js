import fetch from 'isomorphic-fetch';

import { URL_MONGO, URL_STATICS, URL_API, URL_ES, KEY_TYPE } from '../config/const';
import { request } from '../util'


export function getOccurrenceList(offset, search) {
  const xhr = new XMLHttpRequest()
  if (search.indexOf('?') >= 0) {
    search = search.slice(1)
  }

  return request(xhr, 'GET', `${URL_API}/api/resource/searchlite?keyType=${KEY_TYPE}&page=${offset}${search && '&' + search}`)
}


export function ESgetOccurrenceList(offset, search) {
  const xhr = new XMLHttpRequest()
  console.log("Intentando ESgetOccurrenceList")
  if (search.indexOf('?') >= 0) {
    search = search.slice(1)
  }

  console.log("Intentando ESgetOccurrenceList request")
  return request(xhr, 'GET', `${URL_ES}/api/search?keyType=${KEY_TYPE}&page=${offset}${search && '&' + search}`)
}

export function descargar(search, usuario) {
  if (search.indexOf('?') >= 0) {
    search = search.slice(1)
  }

  return fetch(`${URL_API}/api/search/download?email=${usuario}${search && '&' + search}`, {
    method: 'GET'
  }).then((response) => {
    return response.json()
  }).then((data) => {
    return data
  });
}

export function getOccurrence(id) {
  return fetch(`${URL_MONGO}/api/occurrence/` + id, {
    method: 'GET'
  }).then((response) => {
    return response.json()
  }).then((data) => {
    return data
  })
}
export function getThumb(img) {
  let ps = img.split(".");
  if (ps[ps.length - 1] === "svg") {
    return img;
  }

   
//img = img.replace("http:", "").replace("https:", "")
//return 'https://images.weserv.nl/?url=' + encodeURI(img) + "&w=400";
  
  //img = img.replace("http://", "").replace("https://", "")
  return 'http://api.catalogo.biodiversidad.co/api/imageproxy/400x,q60/' + encodeURI(img) + "";
}
export function getThumbID(d){
 
  return fetch(`${URL_STATICS}/api/occurrence/image/${d.occurrenceID}`, {
    method: 'GET'
  }).then((response) => {
    return response.json()
  }).then((data) => {
    return data
  });
  
  

}

