import { URL_ES, KEY_TYPE } from '../config/const';
import { request } from '../util'

const xhr = new XMLHttpRequest()

export function getStats(id, param) {
  let url = URL_ES + '/api/stats'
  if (typeof id !== 'undefined' && typeof param !== 'undefined'){
    url += `?keyType=${KEY_TYPE}&${id}=${param}`
  }else{
    url += `?keyType=${KEY_TYPE}`
  }

  return request(xhr, 'GET', url)
}
