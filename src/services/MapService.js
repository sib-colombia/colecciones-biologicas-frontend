import fetch from 'isomorphic-fetch'
import { join } from 'lodash'

import { URL_GEOJSON, URL_API, URL_ES, KEY_TYPE } from '../config/const'
import { request } from '../util'

const xhr = new XMLHttpRequest()

export function getGeoJson(jsonName, path) {
  return fetch(`${URL_GEOJSON}/mapas/${path}/${jsonName}.geojson`, 
      { method: 'GET' })
  .then((response) => {
    return response.json();
  })
  .then((data) => data)
}

export function getOccurrenceList(query, search) {
  if(query !== undefined && query.length > 0) {
    query = '&' + join(query, '&')
  } else {
    query = search
  }

  return request(xhr, 'GET', `${URL_ES}/api/map?keyType=${KEY_TYPE}&${query}`)
}
